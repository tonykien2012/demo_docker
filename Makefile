build:
	docker build --no-cache -t docker_demo .
up:
	docker-compose -f docker-compose.yml -p docker_demo up

restart:
	docker-compose -f docker-compose.yml -p docker_demo restart
reset:
	docker-compose -p docker_demo down
	docker-compose -p docker_demo up
