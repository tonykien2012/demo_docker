# import json
# import logging
# from celery import bootsteps
# from kombu import Queue, Consumer
# from kombu.exceptions import MessageStateError
# from rest_framework import status
# from tasks.celery import app, camunda_transaction_queue, camunda_cases_queue,\
#     cases_queue, celery_transaction_queue, celery_cases_queue
# from tasks.helpers import check_if_dict, check_allowed_fields, check_required_fields, log_info, log_error
# from tasks.errors import TasksError
# from tasks.tasks import screening_worker

# logger = logging.getLogger(__name__)


# class CeleryCasesConsumerStep(bootsteps.ConsumerStep):

#     def get_consumers(self, channel):
#         return [Consumer(channel,
#                          queues=[celery_cases_queue],
#                          callbacks=[self.handle_message],
#                          accept=['json'])]

#     def handle_message(self, body, message):
#         flag_error = False
#         log_info(logger, "handle_message_celery_cases_queue",
#                          "--- Handle message from queue ---")
#         try:
#             try:
#                 check_if_dict(body, "body")
#             except TasksError:
#                 log_info(logger, "handle_message_celery_cases_queue",
#                          "Try parse body of message to dictionary.")
#                 try:
#                     body = json.loads(body)
#                 except Exception:
#                     flag_error = True
#                     log_error(logger, "handle_message_celery_cases_queue",
#                               "Body of message is not a properly formatted dictionary.")
#             transaction_id = body["transaction_id"]
#             screening_worker.delay(
#                 transaction_id)

#         except MessageStateError:
#             flag_error = True
#             log_error(logger, "handle_message_celery_cases_queue",
#                       "The message has already been acknowledged/requeued/rejected")
#         except Exception as e:
#             flag_error = True
#             log_error(logger, "handle_message_celery_cases_queue", str(e))
#         if flag_error:
#             log_error(logger, "handle_message_celery_cases_queue",
#                       "Message is error, so it will be acknowledged")
#         else:
#             log_info(logger, "handle_message_celery_cases_queue",
#                      "Message will be acknowledged")
#         message.ack()

# Consumer for test receive message when done


# class CamundaCasesConsumerStep(bootsteps.ConsumerStep):

#     def get_consumers(self, channel):
#         return [Consumer(channel,
#                          queues=[camunda_cases_queue],
#                          callbacks=[self.handle_message],
#                          accept=['json'])]

#     def handle_message(self, body, message):
#         print(
#             'Received message from camunda transaction cases: {0!r}'.format(body))
#         print(
#             'Received message from camunda transaction cases: {0!r}'.format(message))
#         message.ack()

# example to send message to queue

# def send_message_to_queue_transaction(message, producer=None):
#     with app.producer_or_acquire(producer) as producer:
#         producer.publish(
#             message,
#             serializer='json',
#             exchange=camunda_transaction_queue.exchange,
#             routing_key=QUEUE_PREFIX+Q_CAMUNDA_TRANSACTION,
#             declare=[camunda_transaction_queue],
#             retry=True,
#             retry_policy={
#                 'interval_start': 0,  # First retry immediately,
#                 'interval_step': 2,  # then increase by 2s for every retry.
#                 'interval_max': 30,  # but don't exceed 30s between retries.
#                 'max_retries': 5,   # give up after 30 tries.
#             })
