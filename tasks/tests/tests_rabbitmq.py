# from django.test import SimpleTestCase
# from kombu.exceptions import MessageStateError
# from mock import Mock, patch
# from tasks.rabbitmq import CeleryCasesConsumerStep, logger


# class MockParent(object):
#     pass


# class MockMessage(object):
#     def __init__(self):
#         self = "message"

#     def ack(self):
#         return
#     pass


# class RabbitMQTest(SimpleTestCase):
#     def setUp(self):
#         parent = MockParent()
#         self.consumer = CeleryCasesConsumerStep(parent)

#     def test_handle_message(self):
#         with self.assertLogs(logger=logger, level="INFO") as cm:
#             self.consumer.handle_message({"transaction_id": 1}, MockMessage())
#             log_start_handle_message = "INFO:tasks.rabbitmq:API [handle_message_celery_cases_queue], data --- Handle message from queue ---"
#             log_success_ack_message_expect = "INFO:tasks.rabbitmq:API [handle_message_celery_cases_queue], data Message will be acknowledged"
#             self.assertIn(log_start_handle_message, cm.output)
#             self.assertIn(log_success_ack_message_expect, cm.output)
#         pass

#     def test_handle_message_error(self):
#         with self.assertLogs(logger=logger, level="ERROR") as cm:
#             self.consumer.handle_message(1, MockMessage())
#             log_body_invalid_format = "ERROR:tasks.rabbitmq:API [handle_message_celery_cases_queue], error Body of message is not a properly formatted dictionary."
#             log_error_ack_message_expect = "ERROR:tasks.rabbitmq:API [handle_message_celery_cases_queue], error Message is error, so it will be acknowledged"
#             self.assertIn(log_body_invalid_format, cm.output)
#             self.assertIn(log_error_ack_message_expect, cm.output)
#         pass

#     @patch("tasks.rabbitmq.check_if_dict", side_effect=MessageStateError())
#     def test_handle_messge_state_error(self, mock_check_if_dict):
#         with self.assertLogs(logger=logger, level="ERROR") as cm:
#             self.consumer.handle_message("transaction_id", MockMessage())
#             log_error_message_state = "ERROR:tasks.rabbitmq:API [handle_message_celery_cases_queue], error The message has already been acknowledged/requeued/rejected"
#             self.assertIn(log_error_message_state, cm.output)
