#region mock rule
mock_case_exist = [{"id": 1, "id": 2}]


def mock_fnc_check_rule(transaction_id, ins_rule, bucket_id):
    # Function process rule will return case_ids
    return [1, 2]


#region Mock Rule
class MockRuleNumber_1(object):
    id = 1

    def filter(self, *args, **kwargs):
        return [(500.0, 1), (2000.0, 2)]


class MockRule_1(object):
    id = 1
    rule_number = MockRuleNumber_1()
    rule_parameters = {
        'transaction_count_exceeds': 1,
        'amount_less_than': 9000.0,
        'lookback_days': 3
    }
    active_screening = True


class MockObjectRule(object):
    def first(self):
        return MockRule_1()


class MockRuleNumber_2(object):
    id = 2


class MockRule_2(object):
    id = 2
    rule_number = MockRuleNumber_2()
    rule_parameters = {
        'lookback_days': 3,
        'transaction_count_exceeds': 1,
    }
    active_screening = True


class MockRuleNumber_3(object):
    id = 3


class MockRule_3(object):
    id = 3
    rule_number = MockRuleNumber_3()
    rule_parameters = {
        'lookback_days': 3,
        'beneficiary_count_exceeds': 1,
    }
    active_screening = True


class MockRuleNumber_4(object):
    id = 4


class MockRule_4(object):
    id = 4
    rule_number = MockRuleNumber_4()
    rule_parameters = {
        'lookback_days': 3,
        'sender_count_exceeds': 1,
    }
    active_screening = True


class MockRuleNumber_5(object):
    id = 5


class MockRule_5(object):
    id = 5
    rule_number = MockRuleNumber_5()
    rule_parameters = {
        'lookback_days': 3,
        'aggregated_amount_exceeds': 3000.0,
    }
    active_screening = True


class MockRuleNumber_6(object):
    id = 6


class MockRule_6(object):
    id = 6
    rule_number = MockRuleNumber_6()
    rule_parameters = {
        'lookback_days': 3,
        'lookback_months_to_determine_baseline': 12,
        'amount_exceed_baseline_by_number_of_times': 1
    }
    active_screening = True


class MockRuleNumber_7(object):
    id = 7


class MockRule_7(object):
    id = 7
    rule_number = MockRuleNumber_7()
    rule_parameters = {'lookback_days': 3}
    active_screening = True


class MockRuleNumber_8(object):
    id = 8


class MockRule_8(object):
    id = 8
    rule_number = MockRuleNumber_8()
    rule_parameters = {'lookback_days': 3}
    active_screening = True


class MockRuleNumber_9(object):
    id = 9


class MockRule_9(object):
    id = 9
    rule_number = MockRuleNumber_9()
    rule_parameters = {
        'lookback_days': 3,
        'aggregated_withdrawal_exceeds': 1000.0
    }


class MockRuleNumber_10(object):
    id = 10


class MockRule_10(object):
    id = 10
    rule_number = MockRuleNumber_10()
    rule_parameters = {
        "aggregated_deposit_exceeds": 1000.0,
        "lookback_days": 3,
    }
    active_screening = True


class MockRuleNumber_11(object):
    id = 11


class MockRule_11(object):
    id = 1
    rule_number = MockRuleNumber_11()
    rule_parameters = {'param1': 1, 'param2': 2, 'param3': 3}
    active_screening = True


class MockRuleNumber_12(object):
    id = 12


class MockRule_12(object):
    id = 12
    rule_number = MockRuleNumber_12()
    rule_parameters = {
        "aggregated_amount_exceeds": 2000.0,
        "variance_percent": 20.0,
        "lookback_days": 3
    }
    active_screening = True


class MockRuleNumber_13(object):
    id = 13


class MockRule_13(object):
    id = 13
    rule_number = MockRuleNumber_13()
    rule_parameters = {
        "lookback_months_to_determine_baseline": 12,
        "amount_exceed_baseline_by_number_of_times": 1,
        "lookback_days": 5
    }
    active_screening = True


class MockRuleNumber_14(object):
    id = 14


class MockRule_14(object):
    id = 14
    rule_number = MockRuleNumber_14()
    rule_parameters = {
        "lookback_days": 5,
        "aggregated_amount_exceeds": 5000.0,
        "minimum_transaction_count": 10
    }
    active_screening = True


class MockRuleNumber_15(object):
    id = 15


class MockRule_15(object):
    id = 15
    rule_number = MockRuleNumber_15()
    rule_parameters = {
        "lookback_days": 5,
        "aggregated_amount_exceeds": 5000.0,
        "minimum_transaction_count": 10
    }
    active_screening = True


#endregion


class MockRules(object):
    def all(self):
        return [MockRule_11()]

    def filter(self, *args, **kwargs):
        return [1, 2, 3, 4]


class MockBucket(object):
    id = 1
    bucket_rules = MockRules()


class MockSubjectA(object):
    client_subject_id = "cpa"
    subject_name = "cpa"


class MockSubjectB(object):
    client_subject_id = "cpb"
    subject_name = "cpb"


#region Mock Transaction
class Mock_daily_transactions_summarized_view(object):
    amount_total = 500.0
    amount_count = 2

    def values_list(self, *args, **kwargs):
        return amount_total, amount_count

    def annotate(self, *args, **kwargs):
        return [['sender_id', 'receiver_id', 6000.0]]


class Mock_daily_transactions_summarized_views(object):
    def exclude(self, id__in):
        return [Mock_daily_transactions_summarized_view()]


class MockTransaction(object):
    id = 1
    client_transaction_id = "id2323"
    cp_a = MockSubjectA()
    cp_b = MockSubjectB()
    bucket = MockBucket()
    amount = 50000.0
    transaction_type = "PAY"
    cp_a_country = "China"
    cp_b_country = "Singapore"


class MockTransactionNotPay(object):
    id = 1
    client_transaction_id = "id323"
    cp_a = MockSubjectA()
    cp_b = MockSubjectB()
    bucket = MockBucket()
    amount = 50000.0
    transaction_type = "RECEIVE"


class MockTransactionNotPayDeposit(object):
    id = 1
    client_transaction_id = "id2323"
    cp_a = MockSubjectA()
    cp_b = MockSubjectB()
    bucket = MockBucket()
    amount = 50000.0
    transaction_type = "DEPOSIT"


class MockTransactionsNotPayDeposit(list):
    def __init__(self):
        self.append(MockTransactionNotPayDeposit())

    def exclude(self, id__in):
        return []

    def first(self):
        return MockTransactionNotPayDeposit()

    def values_list(self, *args, **kwargs):
        return [1, 2]


class MockViewDepositChildrenWithSender(object):
    def filter(self, *args, **kwargs):
        return [(3000.0, ), (2000.0, )]

    def first(self):
        return 'transaction_id_1'


class MockTransactionWithDraw(object):
    id = 1
    client_transaction_id = "id323"
    cp_a = MockSubjectA()
    cp_b = MockSubjectB()
    bucket = MockBucket()
    amount = 50000.0
    transaction_type = "WITHDRAW"


class MockTransactionsChildWithDraw(list):
    def __init__(self):
        self.append(MockTransactionWithDraw())
        self.append(MockTransactionWithDraw())

    def exclude(self, id__in):
        return []

    def first(self):
        return self[0]

    def values_list(self, *args, **kwargs):
        return [1, 2]


class MockTransactionsListClientId(object):
    def filter(self, *args, **kwargs):
        return ['transaction_id_1', 'transaction_id_2']

    def first(self):
        return 'transaction_id_1'


class MockFirst(list):
    def __init__(self):
        self.append(MockTransaction())
        self.append(MockTransaction())

    def first(self):
        return self[0]

    def values_list(self, *args, **kwargs):
        return [1, 2]


class MockTransactionsFirstClientId(list):
    def __init__(self):
        self.append(MockTransaction())
        self.append(MockTransaction())

    def filter(self, *args, **kwargs):
        return MockFirst()

    def first(self):
        return MockTransaction()


class MockTransactions(object):
    def first(self):
        return MockTransaction()

    def exclude(self, id__in):
        return []

    def filter(self, *args, **kwargs):
        return [(1, "txn_1"), (2, "txn_2"), (3, "txn_3"), (4, "txn_4")]

    def values_list(self, *args, **kwargs):
        return


class MockTransactionsChild(list):
    def __init__(self):
        self.append(MockTransactionWithDraw())
        self.append(MockTransactionWithDraw())

    def first(self):
        return MockTransactionWithDraw()

    def filter(self, *args, **kwargs):
        return MockTransactionsChildWithDraw()


class MockTransactionCountryNK(object):
    id = 1
    client_transaction_id = "id323"
    cp_a = MockSubjectA()
    cp_b = MockSubjectB()
    bucket = MockBucket()
    amount = 50000.0
    transaction_type = "PAY"
    cp_a_country = "ANGUILLA"
    cp_b_country = "NORTH KOREA"


class MockTransactionCountrySG(object):
    id = 1
    client_transaction_id = "id323"
    cp_a = MockSubjectA()
    cp_b = MockSubjectB()
    bucket = MockBucket()
    amount = 50000.0
    transaction_type = "PAY"
    cp_a_country = "SINGAPORE"
    cp_b_country = "CHINA"


class MockTransactionsCountryViolate(object):
    def first(self):
        return MockTransactionCountryNK()

    def filter(self, *args, **kwargs):
        return MockTransactionCountryNK()


class MockTransactionsCountryNotViolate(object):
    def first(self):
        return MockTransactionCountrySG()

    def filter(self, *args, **kwargs):
        return MockTransactionCountrySG()


class MockTransactionsNotPay(object):
    def first(self):
        return MockTransactionNotPay()

    def exclude(self, id__in):
        return []

    def filter(self, *args, **kwargs):
        return [1, 2, 3, 4]

    def values_list(self, *args, **kwargs):
        return


class MockViewTransaction(list):
    def filter(self, *args, **kwargs):
        return [(500.0, 1), (2000.0, 2)]

    def annotate(self, *args, **kwargs):
        return [['sender_id', 'receiver_id', 6000.0]]

    def __init__(self, *args, **kwargs):
        self.append((500.0, 1))
        self.append((2000.0, 2))


# Moc rule4


class MockViewTransactionWithSender(object):
    def filter(self, *args, **kwargs):
        return [(500.0, 1), (2000.0, 2)]

    def annotate(self, *args, **kwargs):
        return [['sender_id', 6000.0]]


class MockViewTransactionChildrenWithSender(object):
    def filter(self, *args, **kwargs):
        return MockViewTransactionWithSender()


# Moc rule 9


class MockViewWithDrawChildrenWithSender(list):
    def filter(self, *args, **kwargs):
        return [(3000.0, ), (2000.0, )]

    def first(self):
        return 'transaction_id_1'


# Moc rule 5


class MockViewTransactionRule_5(list):
    def values(self, *args, **kwargs):
        return None

    def filter(self, *args, **kwargs):
        return MockViewTransactionRule_5()

    def annotate(self, *args, **kwargs):
        return [{'bucket_id': 0, 'total': 7000.0}]


class MockTransaction_Rule_5(object):
    def filter(self, *args, **kwargs):
        return MockViewTransactionRule_5()


class MockTransaction_filter_rule_5(list):
    def __init__(self, *args, **kwargs):
        self.append({"bucket_id": 0, "sum_total": 50000.0, "sum_count": 100})
        self.append((2000.0, 2))

    def filter(self, *args, **kwargs):
        return MockTransaction_Rule_5()


class MockTransaction_annotation_rule_5(object):
    def filter(self, *args, **kwargs):
        return MockTransaction_annotation_rule_5()

    def annotate(self, *args, **kwargs):
        return MockTransaction_filter_rule_5()


class MockTransaction_annotation_rule_14(object):
    pass


class MockCasesNone(object):
    def values(self, *args, **kwargs):
        return None

    def values_list(self, *args, **kwargs):
        return None


class MockViewTransactionNotViolate(object):
    def filter(self, *args, **kwargs):
        return [(500000.0, 1), (20000000.0, 2)]


class MockViewTransactionChildren(object):
    def filter(self, *args, **kwargs):
        return MockViewTransaction()


class MockViewTransactionNotViolateChildren(object):
    def filter(self, *args, **kwargs):
        return MockViewTransactionNotViolate()


class MockTransactionsViolate(object):
    def first(self):
        return MockTransaction()

    def exclude(self, id__in):
        return []

    def filter(self, *args, **kwargs):
        return [10000.0, 20000.0, 30000.0, 40000.0]


class MockTransactionsViolateWithdraw(object):
    def first(self):
        return MockTransactionNotPay()

    def exclude(self, id__in):
        return []

    def filter(self, *args, **kwargs):
        return [10000.0, 20000.0, 30000.0, 40000.0]


class MockTransactionsFilter(list):
    def __init__(self):
        self.append(MockTransaction())

    def first(self, *args, **kwargs):
        return MockTransaction()

    def filter(self, *args, **kwargs):
        return MockTransactionsViolate()

    def exclude(self, id__in):
        return []


class MockTransactionsFilterWithdraw(list):
    def __init__(self):
        self.append(MockTransactionNotPay())

    def first(self, *args, **kwargs):
        return MockTransactionNotPay()

    def filter(self, *args, **kwargs):
        return MockTransactionsViolateWithdraw()

    def exclude(self, id__in):
        return []


class MockTransactionsNone(object):
    def first(self):
        return None

    def exclude(self, id__in):
        return []

    def filter(self, *args, **kwargs):
        return []


#endregion


class MockBuckets(object):
    def first(self):
        return MockBucket()

    def exclude(self, id__in):
        return []

    def get(self, *args, **kwargs):
        return MockBucket()


class MockBlackListCpA(object):
    def values(self, *args, **kwargs):
        return [{"name": "cpa"}]


class MockBlackListCpB(object):
    def values(self, *args, **kwargs):
        return [{"name": "cpb"}]


class MockBlackListEmpty(object):
    def values(self, *args, **kwargs):
        return []


class MockCases(object):
    def values(self, *args, **kwargs):
        return [{"id": 1}, {"id": 2}, {"id": 3}]

    def values_list(self, *args, **kwargs):
        return [1, 2, 3]


class MockCasesNone(object):
    def values(self, *args, **kwargs):
        return None

    def values_list(self, *args, **kwargs):
        return None
