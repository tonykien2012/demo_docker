import logging
from django.test import TestCase
from django.core.cache import cache
from mock import Mock, patch
from tasks.helpers import log_info, log_error, send_message_to_queue_transaction_cases, check_if_dict,\
    check_allowed_fields, check_required_fields, find_buckets_by_transaction_id, \
    init_cache_bucket_transaction, init_cache_bucket_rules, update_cache_when_task_done,\
    find_rules_by_bucket, handle_add_transaction_to_transaction_case, check_rule_11, check_if_case_exist_for_subject, check_rule_6,\
    handle_create_case, check_rule_1, check_rule_2, check_rule_3, check_rule_4, check_rule_5,\
    check_rule_7, check_rule_9, check_rule_10, check_rule_12, check_rule_13, check_rule_8,\
        fn_rule_engineer, detect_func_check_rule, interface_check_rule, func_not_found, check_rule_14, check_rule_15
from tasks.errors import TasksError
from django.conf import settings
from tasks.tests.data.data_test_helpers import *
logger = logging.getLogger("logger_test")

field_required_of_message = ["transaction_id", "case_ids"]
field_allowed_of_message = ["transaction_id", "case_ids"]


class HelpersTest(TestCase):
    def tests_log_info(self):
        with self.assertLogs(logger="logger_test", level="INFO") as cm:
            log_info(logger, "/healthCheck", "Ok")
            self.assertIn("INFO:logger_test:API [/healthCheck], [Data] Ok",
                          cm.output)

    def tests_log_error(self):
        with self.assertLogs(logger="logger_test", level="ERROR") as cm:
            log_error(logger, "/healthCheck", "Internal Server")
            self.assertIn(
                "ERROR:logger_test:API [/healthCheck], [Error] Internal Server",
                cm.output)

    def test_check_if_dict_invalid_dict(self):
        invalid_dict = "invalid_dict"
        with self.assertRaises(TasksError):
            check_if_dict(invalid_dict, "message")

    def test_check_allowed_fields_invalid_field(self):
        invalid_dict = {
            "transaction_id": 1,
            "cases_id": [1, 2, 3],
            "field_not_allowed": 1
        }
        with self.assertRaises(TasksError):
            check_allowed_fields(field_allowed_of_message, invalid_dict,
                                 "message")

    def test_check_required_fields_missing_field(self):
        invalid_dict = {
            "transaction_id": 1,
        }
        with self.assertRaises(TasksError):
            check_required_fields(field_required_of_message, invalid_dict,
                                  "message")

    def test_send_message_to_queue_transaction_cases(self):
        message = {"transaction_id": 1, "case_ids": [1, 2, 3]}
        send_message_to_queue_transaction_cases(message)
        self.assertTrue(True)

    @patch("bucket.models.Transaction.objects.filter")
    def test_find_buckets_by_transaction_id(self, mock_transaction_filter):
        mock_transaction_filter.return_value = [MockTransaction()]
        response = find_buckets_by_transaction_id("id232323")
        exepcted_response = {
            'transaction_id': "id232323",
            'bucket_id_list': [1]
        }
        self.assertEqual(response, exepcted_response)

    @patch("bucket.models.Transaction.objects.filter")
    def test_find_buckets_by_transaction_id_invalid(self,
                                                    mock_transaction_filter):
        mock_transaction_filter.return_value = []
        with self.assertRaises(TasksError):
            find_buckets_by_transaction_id(1)

    def test_init_cache_bucket_transaction(self):
        init_cache_bucket_transaction(1, 1, "screening_worker_1")
        response = cache.get("screening_worker_1")

        self.assertIsNotNone(response)
        self.assertEqual(response["transaction_id"], 1)

    def test_init_cache_bucket_rules(self):
        init_cache_bucket_rules("screening_worker_1", 1, 1,
                                "screening_worker_1_bucket_scanning_worker_1")
        response = cache.get("screening_worker_1_bucket_scanning_worker_1")

        self.assertIsNotNone(response)
        self.assertEqual(response["bucket_id"], 1)

    @patch("screening.models.Rule.objects.filter",
           return_value=MockObjectRule())
    @patch("tasks.helpers.detect_func_check_rule",
           return_value=mock_fnc_check_rule)
    def test_fn_rule_engineer(self, mock_rule, mock_fnc_check_rule):
        transaction_id = 1
        rule_id = 1
        bucket_id = 0
        key_cache_screening = "KEY_CACHE_SCREENING"
        response = fn_rule_engineer(transaction_id, rule_id, bucket_id,
                                    key_cache_screening)
        expect_response = {
            "transaction_id": 1,
            "bucket_id": 0,
            "rule_id": 1,
            "case_ids": [1, 2]
        }
        self.assertEqual(response, expect_response)

    def test_detect_func_check_rule(self):
        rule_number = 1
        fnc_check_rule_1 = detect_func_check_rule(rule_number)
        expect_response = "check_rule_1"
        self.assertEqual(expect_response, fnc_check_rule_1.__name__)

    def test_detect_func_check_rule_not_found(self):
        rule_number = 99
        fnc_not_found = detect_func_check_rule(rule_number)
        expect_response = "func_not_found"
        self.assertEqual(expect_response, fnc_not_found.__name__)

    def test_interface_check_rule(self):
        response = interface_check_rule(1, 1, 0)
        self.assertEqual(response, [])

    def test_func_not_found(self):
        response = func_not_found(1, MockRule_1(), 0)
        self.assertEqual(response, [])

    def test_update_cache_when_task_done(self):
        init_cache_bucket_transaction(1, 1, "screening_worker_1")
        cache_bucket_transactions = cache.get("screening_worker_1")
        self.assertIsNotNone(cache_bucket_transactions)
        init_cache_bucket_rules("screening_worker_1", 1, 1,
                                "screening_worker_1_bucket_scanning_worker_1")
        update_cache_when_task_done(
            "screening_worker_1",
            "screening_worker_1_bucket_scanning_worker_1", [1])
        cache_bucket_transactions = cache.get("screening_worker_1")
        self.assertTrue(True)
        self.assertIsNone(cache_bucket_transactions)

    @patch("screening.models.Rule.objects.filter")
    def test_find_rules_by_bucket(self, mock_rules):
        mock_rules.return_value = [MockRule_11()]
        response = find_rules_by_bucket(["id232323"], 1)
        expect_response = [{
            'transaction_id': ["id232323"],
            'bucket_id': 1,
            'rule_id': 1
        }]
        self.assertEqual(response, expect_response)

    @patch("case.models.TransactionCase.objects.get_or_create",
           return_value=(True, True))
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactions())
    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    def test_handle_save_transaction_case(self, mock_transaction_case_model,
                                          mock_transaction_filter,
                                          mock_bucket_models):
        client_transaction_id = "mock_client_transaction_id"
        case_ids = [1, 2, 3]
        response = handle_add_transaction_to_transaction_case(
            client_transaction_id, case_ids, mock_bucket_models)
        expect_response = [1, 2, 3]
        self.assertEqual(response, expect_response)

    @patch("case.models.Case.objects.filter", return_value=MockCases())
    def test_check_if_case_exist_for_subject(self, mock_case_filter):
        response = check_if_case_exist_for_subject("mock_rule", "mock_bucket",
                                                   "subject_1", "subject_2")
        expect_response = [1, 2, 3]
        self.assertEqual(response, expect_response)

    @patch("case.models.Case.objects.filter", return_value=MockCasesNone())
    def test_check_if_case_exist_for_subject_mock_none(self, mock_case_filter):
        response = check_if_case_exist_for_subject("mock_rule", "mock_bucket",
                                                   "subject_1", "subject_2")
        expect_response = None
        self.assertEqual(response, expect_response)

    def test_handle_create_case(self):
        from screening.models import Rule
        from bucket.models import Bucket, Subject
        from case.models import TransactionCase
        rule = Rule.objects.filter(id=11).first()
        bucket = Bucket.objects.get(id=0)
        case_status = "New"
        case_subject_1, created = Subject.objects.get_or_create(
            client_subject_id="CL001", subject_name="CL001")

        case_id = handle_create_case("tx002002", rule, bucket, case_status,
                                     case_subject_1)

        self.assertTrue(case_id > 0)
        self.assertTrue(TransactionCase.objects.count() > 0)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactions())
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactions())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=True)
    def test_check_rule_1(self, mock_bucket, mock_transaction_filter,
                          mock_transaction_values_list, mock_check,
                          mock_handle_add, mock_add_txn_case):
        transaction_id = "client_transaction_id"
        ins_rule = MockRule_1()
        bucket_id = 1
        response = check_rule_1(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)
        pass

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactions())
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactions())
    @patch("tasks.helpers.check_if_case_exist_for_subject",
           return_value=mock_case_exist)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1, 2])
    def test_check_rule_1_exist_in_db(self, mock_bucket,
                                      mock_transaction_filter,
                                      mock_transaction_values_list, mock_check,
                                      mock_handle_add):
        transaction_id = "client_transaction_id"
        ins_rule = MockRule_1()
        bucket_id = 1
        response = check_rule_1(transaction_id, ins_rule, bucket_id)
        expect_response = [1, 2]
        self.assertEqual(response, expect_response)
        pass

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactions())
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsNone())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    def test_check_rule_1_empty(self, mock_bucket, mock_transaction_filter,
                                mock_transaction_values_list, mock_check):
        transaction_id = "client_transaction_id"
        ins_rule = MockRule_1()
        bucket_id = 1
        response = check_rule_1(transaction_id, ins_rule, bucket_id)
        expect_response = []
        self.assertEqual(response, expect_response)
        pass

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsFilter())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionChildren())
    @patch("tasks.helpers.check_if_case_exist_for_subject",
           return_value=mock_case_exist)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1, 2])
    def test_check_rule_2_exist_in_db(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_daily_transactions,
                                      mock_check_if_exist,
                                      mock_save_transaction_case):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_2()
        response = check_rule_2(transaction_id, ins_rule, bucket_id)
        expect_response = [1, 2]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsNotPay())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionNotViolateChildren())
    def test_check_rule_2_not_violate(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_daily_transactions):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_2()
        response = check_rule_2(transaction_id, ins_rule, bucket_id)
        expect_response = []
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsFilter())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionChildren())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsListClientId())
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    def test_check_rule_2_violate(self, mock_bucket_filter,
                                  mock_transaction_filter,
                                  mock_daily_transactions, mock_case_filter,
                                  mock_case_cpa,
                                  mock_transaction_list_clientID,
                                  mock_handle_add):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_2()
        response = check_rule_2(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    # rule3

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsFilter())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionChildrenWithSender())
    @patch("tasks.helpers.check_if_case_exist_for_subject",
           return_value=mock_case_exist)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1, 2])
    def test_check_rule_3_exist_in_db(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_daily_transactions,
                                      mock_check_if_exist,
                                      mock_save_transaction_case):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_3()
        response = check_rule_3(transaction_id, ins_rule, bucket_id)
        expect_response = [1, 2]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsNotPay())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionNotViolateChildren())
    def test_check_rule_3_not_violate(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_daily_transactions):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_3()
        response = check_rule_3(transaction_id, ins_rule, bucket_id)
        expect_response = []
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsFilter())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionChildrenWithSender())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsListClientId())
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    def test_check_rule_3_violate(self, mock_bucket_filter,
                                  mock_transaction_filter,
                                  mock_daily_transactions, mock_case_filter,
                                  mock_case_cpa,
                                  mock_transaction_list_clientID,
                                  mock_handle_add):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_3()
        response = check_rule_3(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    # rule4

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsFilter())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionChildrenWithSender())
    @patch("tasks.helpers.check_if_case_exist_for_subject",
           return_value=mock_case_exist)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[2])
    def test_check_rule_4_exist_in_db(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_daily_transactions,
                                      mock_check_if_exist,
                                      mock_save_transaction_case):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_4()
        response = check_rule_4(transaction_id, ins_rule, bucket_id)
        expect_response = [2]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsNotPay())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionNotViolateChildren())
    def test_check_rule_4_not_violate(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_daily_transactions):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_4()
        response = check_rule_4(transaction_id, ins_rule, bucket_id)
        expect_response = []
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsFilter())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionChildrenWithSender())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsListClientId())
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    def test_check_rule_4_violate(self, mock_bucket_filter,
                                  mock_transaction_filter,
                                  mock_daily_transactions, mock_case_filter,
                                  mock_case_cpa,
                                  mock_transaction_list_clientID,
                                  mock_handle_add):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_4()
        response = check_rule_4(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    # miss checking

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsFilter())
    @patch("bucket.models.daily_transactions_summarized_view.objects.values",
           return_value=MockTransaction_annotation_rule_5())
    @patch("tasks.helpers.check_if_case_exist_for_subject",
           return_value=mock_case_exist)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    def test_check_rule_5_exist_in_db(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_daily_transactions,
                                      mock_check_if_exist,
                                      mock_save_transaction_case):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_5()
        response = check_rule_5(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsNotPay())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionNotViolateChildren())
    def test_check_rule_5_not_violate(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_daily_transactions):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_5()
        response = check_rule_5(transaction_id, ins_rule, bucket_id)
        expect_response = []
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsFilter())
    @patch("bucket.models.daily_transactions_summarized_view.objects.values",
           return_value=MockTransaction_annotation_rule_5())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsListClientId())
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    def test_check_rule_5_violate(self, mock_bucket_filter,
                                  mock_transaction_filter,
                                  mock_daily_transactions, mock_case_filter,
                                  mock_case_cpa,
                                  mock_transaction_list_clientID,
                                  mock_handle_add):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        screen_query = [{'bucket_id': 0, 'total': 7000.0}]
        ins_rule = MockRule_5()
        response = check_rule_5(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsFilter())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionChildren())
    @patch("tasks.helpers.check_if_case_exist_for_subject",
           return_value=mock_case_exist)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1, 2])
    def test_check_rule_6_exist_in_db(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_daily_transactions,
                                      mock_check_if_exist,
                                      mock_save_transaction_case):
        transaction_id = "client_transaction_id"
        ins_rule = MockRule_6()
        bucket_id = 1
        response = check_rule_6(transaction_id, ins_rule, bucket_id)
        expect_response = [1, 2]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsFilter())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionNotViolateChildren())
    def test_check_rule_6_not_violate(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_daily_transactions):
        transaction_id = "client_transaction_id"
        ins_rule = MockRule_6()
        bucket_id = 1
        response = check_rule_6(transaction_id, ins_rule, bucket_id)
        expect_response = []
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsFirstClientId())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionChildren())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    def test_check_rule_6_violate(self, mock_bucket_filter,
                                  mock_transaction_filter_filter,
                                  mock_daily_transactions, mock_case_filter,
                                  mock_case_cpa, mock_handle_add):
        # TODO: Improve test
        transaction_id = "client_transaction_id"
        ins_rule = MockRule_6()
        bucket_id = 1
        response = check_rule_6(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsCountryViolate())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsListClientId())
    def test_check_rule_7_violate(self, mock_bucket_filter,
                                  mock_transaction_filter, mock_case_filter,
                                  mock_case_cpa, mock_handle_add,
                                  mock_transaction_list):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_7()
        response = check_rule_7(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsCountryNotViolate())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsListClientId())
    def test_check_rule_7_not_violate(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_case_filter, mock_case_cpa,
                                      mock_handle_add, mock_transaction_list):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_7()
        response = check_rule_7(transaction_id, ins_rule, bucket_id)
        expect_response = []
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsCountryViolate())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[1])
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsListClientId())
    def test_check_rule_7_exist_in_db(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_case_filter, mock_case_cpa,
                                      mock_transaction_list):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_7()
        response = check_rule_7(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsCountryViolate())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsListClientId())
    def test_check_rule_8_violate(self, mock_bucket_filter,
                                  mock_transaction_filter, mock_case_filter,
                                  mock_case_cpa, mock_handle_add,
                                  mock_transaction_list):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_8()
        response = check_rule_8(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsCountryNotViolate())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsListClientId())
    def test_check_rule_8_not_violate(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_case_filter, mock_case_cpa,
                                      mock_handle_add, mock_transaction_list):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_8()
        response = check_rule_8(transaction_id, ins_rule, bucket_id)
        expect_response = []
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsCountryViolate())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[1])
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsListClientId())
    def test_check_rule_8_exist_in_db(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_case_filter, mock_case_cpa,
                                      mock_transaction_list):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_8()
        response = check_rule_8(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    # rule_9

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsChildWithDraw())
    @patch("bucket.models.daily_withdraw_summarized_view.objects.values_list",
           return_value=MockViewWithDrawChildrenWithSender())
    @patch("tasks.helpers.check_if_case_exist_for_subject",
           return_value=mock_case_exist)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    def test_check_rule_9_exist_in_db(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_daily_transactions,
                                      mock_check_if_exist,
                                      mock_save_transaction_case):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_9()
        response = check_rule_9(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects",
           return_value=MockTransactionsFilter())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionNotViolateChildren())
    def test_check_rule_9_not_violate(self, mock_bucket_filter,
                                      mock_transaction_filter,
                                      mock_daily_transactions):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_9()
        response = check_rule_9(transaction_id, ins_rule, bucket_id)
        expect_response = []
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsChildWithDraw())
    @patch("bucket.models.daily_withdraw_summarized_view.objects.values_list",
           return_value=MockViewWithDrawChildrenWithSender())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    def test_check_rule_9_violate(self, mock_bucket_filter,
                                  mock_transaction_filter,
                                  mock_daily_transactions, mock_case_filter,
                                  mock_case_cpa, mock_handle_add):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_9()
        response = check_rule_9(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    # rule 10

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsNotPayDeposit())
    @patch("bucket.models.daily_deposit_summarized_view.objects.values_list",
           return_value=MockViewDepositChildrenWithSender())
    @patch("tasks.helpers.check_if_case_exist_for_subject",
           return_value=mock_case_exist)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    def test_check_rule_10_exist_in_db(self, mock_bucket_filter,
                                       mock_transaction_filter,
                                       mock_daily_transactions,
                                       mock_check_if_exist,
                                       mock_save_transaction_case):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_10()
        response = check_rule_10(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsNotPay())
    @patch("bucket.models.daily_deposit_summarized_view.objects.values_list",
           return_value=MockViewTransactionNotViolateChildren())
    def test_check_rule_10_not_violate(self, mock_bucket_filter,
                                       mock_transaction_filter,
                                       mock_daily_transactions):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_10()
        response = check_rule_10(transaction_id, ins_rule, bucket_id)
        expect_response = []
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsNotPayDeposit())
    @patch("bucket.models.daily_deposit_summarized_view.objects.values_list",
           return_value=MockViewDepositChildrenWithSender())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsListClientId())
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    def test_check_rule_10_violate(self, mock_bucket_filter,
                                   mock_transaction_filter,
                                   mock_daily_transactions, mock_case_filter,
                                   mock_case_cpa,
                                   mock_transaction_list_clientID,
                                   mock_handle_add):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_10()
        response = check_rule_10(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactions())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    @patch("screening.models.Blacklist.objects.all",
           return_value=MockBlackListCpA())
    @patch("tasks.helpers.handle_create_case", return_value=1)
    def test_check_rule_11_blacklist_cp_a(self, mock_bucket_filter,
                                          mock_transaction_filter,
                                          mock_case_filter,
                                          mock_save_transaction_case,
                                          mock_black_list, mock_case_cpa):
        transaction_id = "client_transaction_id"
        ins_rule = MockRule_11()
        bucket_id = 1
        response = check_rule_11(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactions())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[2])
    @patch("screening.models.Blacklist.objects.all",
           return_value=MockBlackListCpB())
    @patch("tasks.helpers.handle_create_case", return_value=2)
    def test_check_rule_11_blacklist_cp_b(self, mock_bucket_filter,
                                          mock_transaction_filter,
                                          mock_case_filter,
                                          mock_save_transaction_case,
                                          mock_black_list, mock_case_cpb):
        transaction_id = "client_transaction_id"
        ins_rule = MockRule_11()
        bucket_id = 2
        response = check_rule_11(transaction_id, ins_rule, bucket_id)
        expect_response = [2]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactions())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[])
    @patch("screening.models.Blacklist.objects.all",
           return_value=MockBlackListEmpty())
    def test_check_rule_11_blacklist_empty(self, mock_bucket_filter,
                                           mock_transaction_filter,
                                           mock_case_filter,
                                           mock_save_transaction_case,
                                           mock_black_list):
        transaction_id = "client_transaction_id"
        ins_rule = MockRule_11()
        bucket_id = 1
        response = check_rule_11(transaction_id, ins_rule, bucket_id)
        expect_response = []
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactions())
    @patch("tasks.helpers.check_if_case_exist_for_subject",
           return_value=mock_case_exist)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1, 2])
    @patch("screening.models.Blacklist.objects.all",
           return_value=MockBlackListCpA())
    def test_check_rule_11_exist_in_db_cp_a(self, mock_bucket_filter,
                                            mock_transaction_filter,
                                            mock_case_filter,
                                            mock_save_transaction_case,
                                            mock_black_list):
        transaction_id = "client_transaction_id"
        ins_rule = MockRule_11()
        bucket_id = 1
        response = check_rule_11(transaction_id, ins_rule, bucket_id)
        expect_response = [1, 2]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactions())
    @patch("tasks.helpers.check_if_case_exist_for_subject",
           return_value=mock_case_exist)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1, 2])
    @patch("screening.models.Blacklist.objects.all",
           return_value=MockBlackListCpB())
    def test_check_rule_11_exist_in_db_cp_b(self, mock_bucket_filter,
                                            mock_transaction_filter,
                                            mock_case_filter,
                                            mock_save_transaction_case,
                                            mock_black_list):
        transaction_id = "client_transaction_id"
        ins_rule = MockRule_11()
        bucket_id = 1
        response = check_rule_11(transaction_id, ins_rule, bucket_id)
        expect_response = [1, 2]
        self.assertEqual(response, expect_response)

    # rule 12

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsChildWithDraw())
    @patch("bucket.models.daily_deposit_summarized_view.objects.values_list",
           return_value=MockViewTransactionChildren())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[1])
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsListClientId())
    @patch("bucket.models.daily_withdraw_summarized_view.objects.values_list",
           return_value=MockViewWithDrawChildrenWithSender())
    def test_check_rule_12_exist_in_db(self, mock_bucket_filter,
                                       mock_transaction_filter,
                                       mock_daily_transactions,
                                       mock_black_list, mock_handle_add,
                                       mock_value_list_transcations,
                                       mock_daily_witdrawn):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_12()
        response = check_rule_12(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsNotPay())
    @patch("screening.models.Blacklist.objects.all",
           return_value=MockBlackListEmpty())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionChildren())
    def test_check_rule_12_not_violate(self, mock_bucket_filter,
                                       mock_black_list_empty,
                                       mock_transaction_filter,
                                       mock_daily_transactions):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_12()
        response = check_rule_12(transaction_id, ins_rule, bucket_id)
        expect_response = []
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsChildWithDraw())
    @patch("bucket.models.daily_deposit_summarized_view.objects.values_list",
           return_value=MockViewTransactionChildren())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsListClientId())
    @patch("bucket.models.daily_withdraw_summarized_view.objects.values_list",
           return_value=MockViewWithDrawChildrenWithSender())
    def test_check_rule_12_violate(self, mock_bucket_filter,
                                   mock_transaction_filter,
                                   mock_daily_transactions, mock_case_filter,
                                   mock_case_cpa, mock_handle_add,
                                   mock_transaction_list, mock_daily_witdrawn):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_12()
        response = check_rule_12(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)


# rule_13

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsChild())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionChildren())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[1])
    @patch("screening.models.Blacklist.objects.all",
           return_value=MockBlackListCpA())
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    def test_check_rule_13_exist_in_black_list_cpa(self, mock_bucket_filter,
                                                   mock_transaction_filter,
                                                   mock_daily_transactions,
                                                   mock_case_cpa,
                                                   mock_black_list,
                                                   mock_handle_add):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_13()
        response = check_rule_13(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsChild())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionChildren())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[2])
    @patch("screening.models.Blacklist.objects.all",
           return_value=MockBlackListCpB())
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[2])
    def test_check_rule_13_exist_in_black_list_cpa(self, mock_bucket_filter,
                                                   mock_transaction_filter,
                                                   mock_daily_transactions,
                                                   mock_case_cpa,
                                                   mock_black_list,
                                                   mock_handle_add):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_13()
        response = check_rule_13(transaction_id, ins_rule, bucket_id)
        expect_response = [2]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsChild())
    @patch("screening.models.Blacklist.objects.all",
           return_value=MockBlackListEmpty())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionChildren())
    def test_check_rule_13_not_in_black_list(self, mock_bucket_filter,
                                             mock_black_list_empty,
                                             mock_transaction_filter,
                                             mock_daily_transactions):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_13()
        response = check_rule_13(transaction_id, ins_rule, bucket_id)
        expect_response = []
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsChild())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionChildren())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("screening.models.Blacklist.objects.all",
           return_value=MockBlackListCpA())
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    def test_check_rule_13_violate_cpa(self, mock_bucket_filter,
                                       mock_transaction_filter,
                                       mock_daily_transactions,
                                       mock_case_filter, mock_case_cpa,
                                       mock_black_list, mock_handle_add):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_13()
        response = check_rule_13(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsChild())
    @patch(
        "bucket.models.daily_transactions_summarized_view.objects.values_list",
        return_value=MockViewTransactionChildren())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("tasks.helpers.handle_create_case", return_value=2)
    @patch("screening.models.Blacklist.objects.all",
           return_value=MockBlackListCpB())
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[2])
    def test_check_rule_13_violate_cpb(self, mock_bucket_filter,
                                       mock_transaction_filter,
                                       mock_daily_transactions,
                                       mock_case_filter, mock_case_cpa,
                                       mock_black_list, mock_handle_add):
        transaction_id = "client_transaction_id"
        bucket_id = 1
        ins_rule = MockRule_13()
        response = check_rule_13(transaction_id, ins_rule, bucket_id)
        expect_response = [2]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.daily_transactions_summarized_view.objects.values",
           return_value=MockTransaction_annotation_rule_5())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockFirst())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsListClientId())
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    def test_check_rule_14_violate(self, mock_bucket_filter,
                                   mock_daily_transactions, mock_transactions,
                                   mock_check_if_exist,
                                   mock_transaction_initials, mock_create_case,
                                   mock_handle_add):
        transaction_id = 1
        bucket_id = 1
        ins_rule = MockRule_14()
        response = check_rule_14(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.daily_transactions_summarized_view.objects.values",
           return_value=MockTransaction_annotation_rule_5())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockFirst())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[2])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[2])
    def test_check_rule_14_violate_exist_in_db(self, mock_bucket_filter,
                                               mock_daily_transactions,
                                               mock_check_if_exist,
                                               mock_transaction_initials,
                                               mock_create_case,
                                               mock_handle_add):
        transaction_id = 1
        bucket_id = 1
        ins_rule = MockRule_14()
        response = check_rule_14(transaction_id, ins_rule, bucket_id)
        expect_response = [2]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsFilterWithdraw())
    def test_check_rule_14_not_violate(self, mock_bucket_filter,
                                       mock_get_transactions):
        transaction_id = 1
        bucket_id = 1
        ins_rule = MockRule_14()
        response = check_rule_14(transaction_id, ins_rule, bucket_id)
        expect_response = []
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.daily_transactions_summarized_view.objects.values",
           return_value=MockTransaction_annotation_rule_5())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockFirst())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[])
    @patch("bucket.models.Transaction.objects.values_list",
           return_value=MockTransactionsListClientId())
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[1])
    def test_check_rule_15_violate(self, mock_bucket_filter,
                                   mock_daily_transactions, mock_transactions,
                                   mock_check_if_exist,
                                   mock_transaction_initials, mock_create_case,
                                   mock_handle_add):
        transaction_id = 1
        bucket_id = 1
        ins_rule = MockRule_15()
        response = check_rule_15(transaction_id, ins_rule, bucket_id)
        expect_response = [1]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.daily_transactions_summarized_view.objects.values",
           return_value=MockTransaction_annotation_rule_5())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockFirst())
    @patch("tasks.helpers.check_if_case_exist_for_subject", return_value=[2])
    @patch("tasks.helpers.handle_create_case", return_value=1)
    @patch("tasks.helpers.handle_add_transaction_to_transaction_case",
           return_value=[2])
    def test_check_rule_15_violate_exist_in_db(self, mock_bucket_filter,
                                               mock_daily_transactions,
                                               mock_check_if_exist,
                                               mock_transaction_initials,
                                               mock_create_case,
                                               mock_handle_add):
        transaction_id = 1
        bucket_id = 1
        ins_rule = MockRule_15()
        response = check_rule_15(transaction_id, ins_rule, bucket_id)
        expect_response = [2]
        self.assertEqual(response, expect_response)

    @patch("bucket.models.Bucket.objects.filter", return_value=MockBuckets())
    @patch("bucket.models.Transaction.objects.filter",
           return_value=MockTransactionsFilterWithdraw())
    def test_check_rule_15_not_violate(self, mock_bucket_filter,
                                       mock_get_transactions):
        transaction_id = 1
        bucket_id = 1
        ins_rule = MockRule_15()
        response = check_rule_15(transaction_id, ins_rule, bucket_id)
        expect_response = []
        self.assertEqual(response, expect_response)
