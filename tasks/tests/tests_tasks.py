from django.test import TestCase
from mock import Mock, patch

from tasks.tasks import screening_worker, bucket_scanning_worker, CallbackRuleChecking, rule_checking_worker

mock_cache_get = {
    "transaction_id": 1,
    "total_buckets": 1,
    "total_buckets_screened": 0,
    "case_ids": []
}

mock_bucket_transaction_id = {"transaction_id": 1, "bucket_id_list": [1, 2, 3]}

mock_transaction_relevant = [{
    "transaction_id_list": [1],
    "bucket_id": 1
}, {
    "transaction_id_list": [3],
    "bucket_id": 2
}, {
    "transaction_id_list": [2],
    "bucket_id": 3
}]

mock_all_rules = [{
    "transaction_id": 1,
    "bucket_id": 1,
    "rule_id": 1,
}]


class TasksTest(TestCase):
    def setUp(self):
        self.instance_callback_rule = CallbackRuleChecking()
        self.retval = 1
        self.task_id = "17ebefcd-202c-4564-9440-2c64eb0c5ee6"
        self.args = [1, 0, 2, 3, 4]

    @patch("tasks.tasks.update_cache_when_task_done", return_value=True)
    def test_callback_on_success_checking_rule(self, mock_update_cache):
        self.instance_callback_rule.on_success(self.retval, self.task_id,
                                               self.args, [])
        self.assertTrue(True)
        pass

    @patch("tasks.tasks.find_buckets_by_transaction_id",
           return_value=mock_bucket_transaction_id)
    def test_screening_worker(self, mock_find_transaction):
        response = screening_worker("id232323")
        self.assertTrue(response)

    @patch("tasks.tasks.find_rules_by_bucket", return_value=mock_all_rules)
    @patch("tasks.tasks.rule_checking_worker", return_value=True)
    def test_bucket_scanning_worker(self, mock_find_rules, mock_checking_rule):
        response = bucket_scanning_worker([1, 2], 1, "screening_worker_1")
        self.assertTrue(response)

    @patch("tasks.tasks.fn_rule_engineer",
           return_value={
               "transaction_id": 1,
               "bucket_id": 0,
               "rule_id": 1,
               "case_ids": [1, 2, 3]
           })
    def test_rule_checking_worker(self, mock_fn_rule):
        response = rule_checking_worker(1, 1, 0, "key_cache_screening",
                                        "key_cache_bucket")
        expect_response = [1, 2, 3]
        self.assertEqual(response, expect_response)
