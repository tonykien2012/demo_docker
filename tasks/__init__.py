from __future__ import absolute_import, unicode_literals

from .celery import app as celery_app
# from .rabbitmq import CeleryCasesConsumerStep
# from .rabbitmq CamundaCasesConsumerStep

# celery_app.steps['consumer'].add(CeleryCasesConsumerStep)
# celery_app.steps['consumer'].add(CamundaCasesConsumerStep)

__all__ = ('celery_app',)
