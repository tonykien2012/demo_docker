import logging

from celery import task
from tasks.helpers import find_buckets_by_transaction_id, \
    find_rules_by_bucket, update_cache_when_task_done, init_cache_bucket_rules, init_cache_bucket_transaction, fn_rule_engineer

logger = logging.getLogger(__name__)


class CallbackRuleChecking(task.Task):
    def on_success(self, retval, task_id, args, kwargs):
        # result of bucket scanning
        result_case_ids = retval
        # params get from task
        key_cache_screening = args[3]
        key_cache_bucket_scanning = args[4]
        update_cache_when_task_done(key_cache_screening,
                                    key_cache_bucket_scanning, result_case_ids)

    def on_failure(self, exc, task_id, args, kwargs, einfo):
        pass

    def on_retry(self, exc, task_id, args, kwargs, einfo):
        pass


@task(name="screening", track_started=True)
def screening_worker(transaction_id):
    '''
        cache_bucket_transactions is saved in cache with key is transaction_id.
        cache_bucket_transactions: {
            transaction_id: 1 # base_transaction.
            total_buckets: 5 # total bucket of base_transaction & relevant transactions.
            total_buckets_screened: 1 # total buckets have been screened.
            case_ids: [] # Array case id <int> - case id is created by bucket scanning
        }
    '''
    buckets_transaction = find_buckets_by_transaction_id(transaction_id)
    base_transaction_id = buckets_transaction["transaction_id"]
    bucket_id_list = buckets_transaction["bucket_id_list"]
    key_transaction_ids = buckets_transaction["key_transaction_ids"]
    # all_buckets = find_transaction_relevant_by_bucket(
    #     bucket_id_list, base_transaction_id)

    for bucket_id in bucket_id_list:
        logger.info('calling init cache')
        key_cache = "screening_worker_" + str(key_transaction_ids[bucket_id])
        init_cache_bucket_transaction(transaction_id, len(bucket_id_list),
                                    key_cache)
        logger.info('called init cache')
        logger.info('# Call bucket scanning')
        bucket_scanning_worker.delay(base_transaction_id, bucket_id, key_cache)
        logger.info('# Bucket scanning called')
        key_cache = ""
    return True


@task(name="bucket_scanning", track_started=True)
def bucket_scanning_worker(transaction_id, bucket_id, key_cache_screening):
    all_rules = find_rules_by_bucket(transaction_id, bucket_id)
    key_cache = key_cache_screening + \
        "_bucket_scanning_worker_" + str(bucket_id)
    init_cache_bucket_rules(key_cache_screening, bucket_id, len(all_rules),
                            key_cache)
    for rule in all_rules:
        transaction_id = rule["transaction_id"]
        bucket_id = rule["bucket_id"]
        rule_id = rule["rule_id"]
        rule_checking_worker.apply(args=[
            transaction_id, bucket_id, rule_id, key_cache_screening, key_cache
        ])
    return True


@task(name="rule_checking_worker",
      base=CallbackRuleChecking,
      track_started=True)
def rule_checking_worker(transaction_id, bucket_id, rule_id,
                         key_cache_screening, key_cache_bucket_scanning):
    case_ids = []
    result_checking_rule = fn_rule_engineer(transaction_id, rule_id, bucket_id,
                                            key_cache_screening)
    result_case_ids = result_checking_rule["case_ids"]
    case_ids = case_ids + \
        list(set(result_case_ids)-set(case_ids))
    return case_ids
