from rest_framework.exceptions import APIException, _get_error_details
from rest_framework import status
from django.utils.translation import gettext_lazy as _


class TasksError(APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = _('Task error')
    default_code = "task_error"

    def __init__(self, code, message, default_code):
        self.status_code = code
        self.detail = _get_error_details(message, default_code)
