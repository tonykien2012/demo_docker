import logging
from datetime import timedelta
from django.core.cache import cache
from django.db.models import Q
from django.utils.timezone import localtime, now
from rest_framework import status
from demo_docker.settings import QUEUE_PREFIX, Q_CAMUNDA_CASES
from tasks.celery import app, camunda_cases_queue
from tasks.errors import TasksError

logger = logging.getLogger(__name__)

field_required_of_message = ["transaction_id", "case_ids"]
field_allowed_of_message = ["transaction_id", "case_ids"]

TIMEDELTA_OFFSET_TZ = localtime(now()).utcoffset()

HOUR_OFFSET_TZ = TIMEDELTA_OFFSET_TZ.seconds // 3600

HOUR_OFFSET_TZ = HOUR_OFFSET_TZ if TIMEDELTA_OFFSET_TZ.days == 0 else HOUR_OFFSET_TZ - 24


def log_info(log, api_url, data):
    log.info('API [%s], [Data] %s' % (api_url, data))


def log_error(log, api_url, error):
    log.error('API [%s], [Error] %s' % (api_url, error))


def send_message_to_queue_transaction_cases(message, producer=None):
    '''
    send message to queue transaction_cases
    args:
        message: type dict
            example: {
                transaction_id: 1,
                case_ids: [1,2,3]
            }
    '''
    check_if_dict(message, "message")
    check_allowed_fields(field_allowed_of_message, message, "message")
    check_required_fields(field_required_of_message, message, "message")
    with app.producer_or_acquire(producer) as producer:
        producer.publish(
            message,
            serializer='json',
            exchange=camunda_cases_queue.exchange,
            routing_key=QUEUE_PREFIX + Q_CAMUNDA_CASES,
            declare=[camunda_cases_queue],
            retry=True,
            retry_policy={
                'interval_start': 0,  # First retry immediately,
                'interval_step': 2,  # then increase by 2s for every retry.
                'interval_max': 30,  # but don't exceed 30s between retries.
                'max_retries': 5,  # give up after 30 tries.
            })


def check_if_dict(obj, field):
    if not isinstance(obj, dict):
        raise TasksError(status.HTTP_400_BAD_REQUEST,
                         "%s is not a properly formatted dictionary" % field,
                         "invalid_dict")


def check_allowed_fields(allowed, obj, obj_name):
    # Check for fields that aren't in spec
    failed_list = [x for x in obj.keys() if x not in allowed]
    if failed_list:
        raise TasksError(
            status.HTTP_400_BAD_REQUEST, "Invalid field(s) found in %s - %s" %
            (obj_name, ', '.join(failed_list)), "invalid_field")


def check_required_fields(required, obj, obj_name):
    for field in required:
        if field not in obj:
            raise TasksError(status.HTTP_400_BAD_REQUEST,
                             "%s is missing in %s" % (field, obj_name),
                             "missing_required_field")


def find_buckets_by_transaction_id(transaction_id):
    '''
        returns: {
            transaction_id: <int>,
            bucket_id_list: [<int_id_1>,<int_id_2>,<int_id_3>]
        }
    '''
    from bucket.models import Transaction
    result = {}
    qs_transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id)
    if qs_transaction:
        result["transaction_id"] = transaction_id
        result["key_transaction_ids"] = {}
        result["bucket_id_list"] = []
        for transaction in qs_transaction:
            # Only screening with bucket have least one rule active_screening
            if len(
                    transaction.bucket.bucket_rules.filter(
                        active_screening=True)) > 0:
                result["bucket_id_list"].append(transaction.bucket.id)
                result["key_transaction_ids"][
                    transaction.bucket.id] = transaction.id
    else:
        raise TasksError(status.HTTP_400_BAD_REQUEST,
                         "transaction_id is invalid.", "invalid_param")
    return result


def find_rules_by_bucket(transaction_id, bucket_id):
    '''
    params: transaction_id_list: [<str_client_tnx_id_base>,<str_client_tnx_id_2>,<str_client_tnx_id_3>],
            bucket_id: <int>
    returns: [
        {
                transaction_id_list: [<str_client_tnx_id_base>,<str_client_tnx_id_2>,<str_client_tnx_id_3>],
                bucket_id: <int>,
                rule_id: <int>,
        }
    ]
    '''
    from screening.models import Rule
    qs_rules = Rule.objects.filter(bucket_id_id=bucket_id)
    result = []
    for rule in qs_rules:
        if rule.active_screening:
            item_rule_checking = {}
            item_rule_checking["transaction_id"] = transaction_id
            item_rule_checking["bucket_id"] = bucket_id
            item_rule_checking["rule_id"] = rule.id
            result.append(item_rule_checking)
    return result


def update_cache_when_task_done(key_cache_screening, key_cache_bucket_scanning,
                                result_case_ids):
    '''
    cache_bucket_rules: {
           bucket_id: 1 # bucket ,
           total_rules: 5 # total rules of bucket,
           total_rules_screened: 1 # total rules have been screened,
           cases_ids: [] # Array case id <int> - case id is created by rule scanning
       }
    cache_bucket_transactions: {
           transaction_id: 1 # base_transaction.
           # total bucket of base_transaction & relevant transactions.
           total_buckets: 5
           total_buckets_screened: 1 # total buckets have been screened.
           case_ids: [] # Array case id <int> - case id is created by bucket scanning
       }
    '''
    log_info(
        logger, "screening_worker",
        "--- UPDATE CACHE --- DATA: key_cache_screening: {0} | key_cache_bucket_scanning: {1} | result: {2}"
        .format(key_cache_screening, key_cache_bucket_scanning,
                result_case_ids))
    cache_bucket_rules = cache.get(key_cache_bucket_scanning)
    total_rules = cache_bucket_rules["total_rules"]
    total_rules_screened = cache_bucket_rules["total_rules_screened"]
    case_ids = cache_bucket_rules["case_ids"]
    # increase total bucket screened by 1
    total_rules_screened.append(1)
    # add case_id if not exist in case_ids
    case_ids = case_ids + \
               list(set(result_case_ids) - set(case_ids))
    cache_bucket_rules["case_ids"] = case_ids
    cache_bucket_rules["total_rules_screened"] = total_rules_screened
    cache.set(key_cache_bucket_scanning, cache_bucket_rules, 86400)
    # Rule checking finish
    if len(total_rules_screened) == total_rules:
        cache_bucket_transactions = cache.get(key_cache_screening)
        total_buckets = cache_bucket_transactions["total_buckets"]
        total_buckets_screened = cache_bucket_transactions[
            "total_buckets_screened"]
        case_ids_in_screening = cache_bucket_transactions["case_ids"]
        # increase total bucket screened by 1
        total_buckets_screened.append(1)
        # add case_id if not exist in case_ids
        combine_case_ids = case_ids + \
                           list(set(case_ids_in_screening) - set(case_ids))
        cache_bucket_transactions["case_ids"] = combine_case_ids
        cache_bucket_transactions[
            "total_buckets_screened"] = total_buckets_screened
        cache.set(key_cache_screening, cache_bucket_transactions, 86400)
        # Done screening flow.
        if len(total_buckets_screened) == total_buckets:
            log_info(logger, "screening_worker", "--- Done Screening ---")

            # Must have case then to send message to rabbitmq
            if len(combine_case_ids) > 0:
                transaction_id = key_cache_screening.split("_")[-1]
                message = {
                    'transaction_id': transaction_id,
                    'case_ids': combine_case_ids
                }
                # Send message to rabbitmq
                send_message_to_queue_transaction_cases(message)
            # Clear cache when done
            cache.delete(key_cache_screening)
        cache.delete(key_cache_bucket_scanning)


def init_cache_bucket_transaction(transaction_id, total_buckets, key_cache):
    # Init cache `cache_bucket_transactions` | key_cache = screening_worker_<transaction_id>
    cache_bucket_transactions = {}
    cache_bucket_transactions["transaction_id"] = transaction_id
    cache_bucket_transactions["total_buckets"] = total_buckets
    cache_bucket_transactions["total_buckets_screened"] = []
    cache_bucket_transactions["case_ids"] = []
    # Cache `cache_bucket_transactions` with time 1 day

    cache.set(key_cache, cache_bucket_transactions, 86400)


def init_cache_bucket_rules(key_cache_screening, bucket_id, total_rules,
                            key_cache):
    # Init cache `cache_bucket_rules` | key_cache = <key_cache_screening>_bucket_scanning_worker_<bucket_id>
    cache_bucket_rules = {}
    cache_bucket_rules["key_cache_screening"] = key_cache_screening
    cache_bucket_rules["bucket_id"] = bucket_id
    cache_bucket_rules["total_rules"] = total_rules
    cache_bucket_rules["total_rules_screened"] = []
    cache_bucket_rules["case_ids"] = []
    # Cache `cache_bucket_rules` with time 1 day

    cache.set(key_cache, cache_bucket_rules, 86400)


def fn_rule_engineer(transaction_id, rule_id, bucket_id, key_cache_screening):
    from screening.models import Rule, Blacklist
    from bucket.models import Transaction, Bucket
    from case.models import Case
    final_result = {
        "transaction_id": transaction_id,
        "bucket_id": bucket_id,
        "rule_id": rule_id,
        "case_ids": []
    }
    # Find rule by rule_id
    ins_rule = Rule.objects.filter(id=rule_id).first()
    # Exist rule & rule is active screening
    if ins_rule and ins_rule.active_screening:
        # get func_check_rule
        fnc_check_rule = detect_func_check_rule(ins_rule.rule_number.id)
        final_result["case_ids"] = fnc_check_rule(transaction_id, ins_rule,
                                                  bucket_id)
    return final_result


def detect_func_check_rule(rule_number):
    rule_mapping = {
        1: check_rule_1,
        2: check_rule_2,
        3: check_rule_3,
        4: check_rule_4,
        5: check_rule_5,
        6: check_rule_6,
        7: check_rule_7,
        8: check_rule_8,
        9: check_rule_9,
        10: check_rule_10,
        11: check_rule_11,
        12: check_rule_12,
        13: check_rule_13,
        14: check_rule_14,
        15: check_rule_15,
    }
    try:
        return rule_mapping[rule_number]
    except KeyError:
        return func_not_found


def interface_check_rule(transaction_id, ins_rule, bucket_id):
    # Function process rule will return case_ids
    case_ids = []
    return case_ids


def func_not_found(transaction_id, ins_rule, bucket_id):
    log_error(
        logger, "tasks/detect_func_check_rule",
        "[ERROR] fnc check rule not found - rule_id: {}".format(
            str(ins_rule.id)))
    return []


def check_rule_1(transaction_id, ins_rule, bucket_id):
    log_info(logger, "tasks/check_rule_1", "--- Start Checking Rule 1 ---")
    from bucket.models import Transaction, Bucket
    # RULE query filter get all Json field
    rule_query = ins_rule.rule_parameters
    transaction_count_exceeds = rule_query["transaction_count_exceeds"]
    amount_less_than = rule_query["amount_less_than"]
    lookback_days = rule_query["lookback_days"]

    case_ids = []
    ins_bucket = Bucket.objects.filter(id=bucket_id).first()
    today = localtime(now())
    start_date = localtime(today - timedelta(days=lookback_days)).replace(
        hour=0, minute=0, second=0, microsecond=0)
    nextday = localtime(today + timedelta(days=1)).replace(hour=0,
                                                           minute=0,
                                                           second=0,
                                                           microsecond=0)
    base_transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id, bucket=ins_bucket).first()
    subject = base_transaction.cp_a

    # Transaction that are not "PAY" will not be screened for this rule.
    # Nor will there be any cases for non-PAY transactions.
    # Also do not screen if transaction is dated outside lookback day range.
    if base_transaction.transaction_type != "PAY" or base_transaction.transaction_timestamp >= nextday or base_transaction.transaction_timestamp < start_date:
        return case_ids

    if subject:
        list_transactions = Transaction.objects.values_list(
            'amount', 'client_transaction_id').filter(
                Q(cp_a=subject) | Q(cp_b=subject), Q(bucket=ins_bucket),
                Q(transaction_type="PAY"), Q(amount__lte=amount_less_than),
                Q(transaction_timestamp__gte=start_date),
                Q(transaction_timestamp__lt=nextday))
        total_list_transaction = len(list_transactions)

        if total_list_transaction > transaction_count_exceeds:
            query_cases = check_if_case_exist_for_subject(
                ins_rule, ins_bucket, subject)

            if len(query_cases) > 0:
                case_ids_subject = handle_add_transaction_to_transaction_case(
                    transaction_id, query_cases, ins_bucket)
                case_ids.extend(case for case in case_ids_subject
                                if case not in case_ids)
            else:
                for i, transaction in enumerate(list_transactions):
                    if i == 0:
                        case_status = "New"
                        case_id_created = handle_create_case(
                            transaction[1], ins_rule, ins_bucket, case_status,
                            subject)
                        case_ids.append(case_id_created)

                    handle_add_transaction_to_transaction_case(
                        transaction[1], case_ids, ins_bucket)
    # Remove duplicate if exist
    return list(set(case_ids))


def check_rule_2(transaction_id, ins_rule, bucket_id):
    log_info(logger, "tasks/check_rule_2", "--- Start Checking Rule 2 ---")
    from bucket.models import Transaction, Bucket, daily_transactions_summarized_view
    from django.db.models import Q, Sum
    # RULE query filter get all Json field
    rule_query = ins_rule.rule_parameters
    transaction_count_exceeds = rule_query["transaction_count_exceeds"]
    lookback_days = rule_query["lookback_days"]
    case_ids = list()

    bucket_record = Bucket.objects.filter(id=bucket_id).first()
    today = localtime(now())
    start_date = localtime(today - timedelta(days=lookback_days)).replace(
        hour=0, minute=0, second=0, microsecond=0)
    nextday = localtime(today + timedelta(days=1)).replace(hour=0,
                                                           minute=0,
                                                           second=0,
                                                           microsecond=0)

    today_tsdb = localtime(today).replace(
        hour=0, minute=0, second=0,
        microsecond=0) + timedelta(hours=HOUR_OFFSET_TZ)
    start_date_tsdb = today_tsdb - timedelta(days=lookback_days)
    nextday_tsdb = today_tsdb + timedelta(days=1)

    base_transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id, bucket=bucket_record).first()
    subject_1 = base_transaction.cp_a
    subject_2 = base_transaction.cp_b

    # Transaction that are not "PAY" will not be screened for this rule.
    # Nor will there be any cases for non-PAY transactions.
    # Also do not screen if transaction is dated outside lookback day range.
    if base_transaction.transaction_type != "PAY" or base_transaction.transaction_timestamp >= nextday or base_transaction.transaction_timestamp < start_date:
        return case_ids
    else:
        # Check via this rule: "Are there multiple transactions from this Remitter to this Bene?"
        screen_query = daily_transactions_summarized_view.objects.values_list(
            'sender_id',
            'receiver_id').filter(Q(sender_id=subject_1.client_subject_id),
                                  Q(receiver_id=subject_2.client_subject_id),
                                  Q(bucket_id=bucket_id),
                                  Q(day__gte=start_date_tsdb),
                                  Q(day__lt=nextday_tsdb)).annotate(
                                      count_total=Sum('amount_count'))

        if len(screen_query
               ) > 0 and screen_query[0][2] > transaction_count_exceeds:
            # Are there any cases for this Remitter-Transmitter pair?
            query_cases = check_if_case_exist_for_subject(ins_rule,
                                                          bucket_record,
                                                          subject_1,
                                                          subject_2=subject_2)

            if query_cases and len(query_cases) > 0:
                case_ids.extend(
                    handle_add_transaction_to_transaction_case(
                        transaction_id, query_cases, bucket_record))
            else:

                initial_records = Transaction.objects.values_list(
                    'client_transaction_id', flat=True).filter(
                        cp_a=subject_1,
                        cp_b=subject_2,
                        bucket=bucket_record,
                        transaction_timestamp__lt=nextday,
                        transaction_timestamp__gte=start_date,
                    )

                # Load any historical record that resulted in the hit to trigger for case creation (assuming no case was created previously)
                for i, record in enumerate(initial_records):
                    # First loop - create new case for base transaction
                    if i == 0:
                        new_case = handle_create_case(transaction_id,
                                                      ins_rule,
                                                      bucket_record,
                                                      "New",
                                                      subject_1,
                                                      case_subject_2=subject_2)
                        # To reach the for loop this line, case_id needs to be empty previously.
                        # Thus no need for an additional list.
                        case_ids.append(new_case)

                    handle_add_transaction_to_transaction_case(
                        record, case_ids, bucket_record)
        return list(set(case_ids))


def check_rule_3(transaction_id, ins_rule, bucket_id):
    log_info(logger, "tasks/check_rule_3", "--- Start Checking Rule 3 ---")
    from bucket.models import Transaction, Bucket, daily_transactions_summarized_view
    from django.db.models import Q, Count
    # RULE query filter get all Json field
    rule_query = ins_rule.rule_parameters
    beneficiary_count_exceeds = rule_query["beneficiary_count_exceeds"]
    lookback_days = rule_query["lookback_days"]
    case_ids = list()

    ins_bucket = Bucket.objects.filter(id=bucket_id).first()
    today = localtime(now())
    start_date = localtime(today - timedelta(days=lookback_days)).replace(
        hour=0, minute=0, second=0, microsecond=0)
    nextday = localtime(today + timedelta(days=1)).replace(hour=0,
                                                           minute=0,
                                                           second=0,
                                                           microsecond=0)

    today_tsdb = localtime(today).replace(
        hour=0, minute=0, second=0,
        microsecond=0) + timedelta(hours=HOUR_OFFSET_TZ)
    start_date_tsdb = today_tsdb - timedelta(days=lookback_days)
    nextday_tsdb = today_tsdb + timedelta(days=1)

    base_transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id, bucket=ins_bucket).first()
    subject_1 = base_transaction.cp_a

    # Transaction that are not "PAY" will not be screened for this rule.
    # Nor will there be any cases for non-PAY transactions.
    # Also do not screen if transaction is dated outside lookback day range.
    if base_transaction.transaction_type != "PAY" or base_transaction.transaction_timestamp >= nextday or base_transaction.transaction_timestamp < start_date:
        return case_ids
    else:
        screen_query = daily_transactions_summarized_view.objects.values_list(
            'sender_id').filter(
                Q(sender_id=subject_1.client_subject_id),
                Q(bucket_id=bucket_id), Q(day__gte=start_date_tsdb),
                Q(day__lt=nextday_tsdb)).annotate(
                    count_total=Count('receiver_id', distinct=True))
        if len(screen_query
               ) > 0 and screen_query[0][1] > beneficiary_count_exceeds:
            query_cases = check_if_case_exist_for_subject(
                ins_rule, ins_bucket, subject_1)
            if query_cases and len(query_cases) > 0:
                case_ids.extend(
                    handle_add_transaction_to_transaction_case(
                        transaction_id, query_cases, ins_bucket))
            else:
                initial_records = Transaction.objects.values_list(
                    'client_transaction_id',
                    flat=True).filter(cp_a=subject_1,
                                      bucket=ins_bucket,
                                      transaction_timestamp__lt=nextday,
                                      transaction_timestamp__gte=start_date,
                                      transaction_type="PAY")

                # Load any historical record that resulted in the hit to trigger for case creation (assuming no case was created previously)
                for i, record in enumerate(initial_records):
                    if i == 0:
                        new_case = handle_create_case(record, ins_rule,
                                                      ins_bucket, "New",
                                                      subject_1)
                        case_ids.append(new_case)
                    else:
                        handle_add_transaction_to_transaction_case(
                            record, case_ids, ins_bucket)
    return list(set(case_ids))


def check_rule_4(transaction_id, ins_rule, bucket_id):
    log_info(logger, "tasks/check_rule_4", "--- Start Checking Rule 4 ---")
    from bucket.models import Transaction, Bucket, daily_transactions_summarized_view
    from django.db.models import Q, Count
    # RULE query filter get all Json field
    rule_query = ins_rule.rule_parameters
    sender_count_exceeds = rule_query["sender_count_exceeds"]
    lookback_days = rule_query["lookback_days"]
    case_ids = list()

    ins_bucket = Bucket.objects.filter(id=bucket_id).first()
    today = localtime(now())
    start_date = localtime(today - timedelta(days=lookback_days)).replace(
        hour=0, minute=0, second=0, microsecond=0)
    nextday = localtime(today + timedelta(days=1)).replace(hour=0,
                                                           minute=0,
                                                           second=0,
                                                           microsecond=0)

    today_tsdb = localtime(today).replace(
        hour=0, minute=0, second=0,
        microsecond=0) + timedelta(hours=HOUR_OFFSET_TZ)
    start_date_tsdb = today_tsdb - timedelta(days=lookback_days)
    nextday_tsdb = today_tsdb + timedelta(days=1)

    base_transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id, bucket=ins_bucket).first()
    subject_2 = base_transaction.cp_b

    # Transaction that are not "PAY" will not be screened for this rule.
    # Nor will there be any cases for non-PAY transactions.
    # Also do not screen if transaction is dated outside lookback day range.
    if base_transaction.transaction_type != "PAY" or base_transaction.transaction_timestamp >= nextday or base_transaction.transaction_timestamp < start_date:
        return case_ids
    else:
        screen_query = daily_transactions_summarized_view.objects.values_list(
            'receiver_id').filter(
                Q(receiver_id=subject_2.client_subject_id),
                Q(bucket_id=bucket_id), Q(day__gte=start_date_tsdb),
                Q(day__lt=nextday_tsdb)).annotate(
                    count_total=Count('sender_id', distinct=True))
        if len(screen_query) > 0 and screen_query[0][1] > sender_count_exceeds:
            query_cases = check_if_case_exist_for_subject(
                ins_rule, ins_bucket, subject_2)
            if query_cases and len(query_cases) > 0:
                case_ids.extend(
                    handle_add_transaction_to_transaction_case(
                        transaction_id, query_cases, ins_bucket))
            else:
                initial_records = Transaction.objects.values_list(
                    'client_transaction_id',
                    flat=True).filter(cp_b=subject_2,
                                      bucket=ins_bucket,
                                      transaction_timestamp__lt=nextday,
                                      transaction_timestamp__gte=start_date,
                                      transaction_type="PAY")

                # Load any historical record that resulted in the hit to trigger for case creation (assuming no case was created previously)
                for i, record in enumerate(initial_records):
                    if i == 0:
                        new_case = handle_create_case(record, ins_rule,
                                                      ins_bucket, "New",
                                                      subject_2)
                        case_ids.append(new_case)
                    else:
                        handle_add_transaction_to_transaction_case(
                            record, case_ids, ins_bucket)
    return list(set(case_ids))


def check_rule_5(transaction_id, ins_rule, bucket_id):
    '''
    1. Find base transaction
    2. Extract the JSON for the rule to retrieve the rule parameter values
    3. Check if case exist for given rule and bucket for cp_a and cp_b
    4. Screen for cp_a and cp_b
    5. If (3) is true, add the transaction to the Case. Otherwise, create a new case and add the transaction to that case.

    transaction_type = [PAY]
    '''
    log_info(logger, "tasks/check_rule_5", "--- Start Checking Rule 5 ---")
    from bucket.models import daily_transactions_summarized_view, Transaction, Bucket
    from django.db.models import Q, Sum

    bucket = Bucket.objects.get(id=bucket_id)

    transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id, bucket=bucket).first()

    # Placeholder to return to calls to check_rule_5() for any new cases created by the transaction
    case_ids = list()
    # Extract the Rule record given the rule_id (not rule number) to find the parameters to use
    rule_query = ins_rule.rule_parameters
    aggregated_amount_exceeds = rule_query["aggregated_amount_exceeds"]
    lookback_days = rule_query["lookback_days"]

    subject_1 = transaction.cp_a
    subject_2 = transaction.cp_b

    today = localtime(now())
    start_date = localtime(today - timedelta(days=lookback_days)).replace(
        hour=0, minute=0, second=0, microsecond=0)
    nextday = localtime(today + timedelta(days=1)).replace(hour=0,
                                                           minute=0,
                                                           second=0,
                                                           microsecond=0)
    today_tsdb = localtime(today).replace(
        hour=0, minute=0, second=0,
        microsecond=0) + timedelta(hours=HOUR_OFFSET_TZ)
    start_date_tsdb = today_tsdb - timedelta(days=lookback_days)
    nextday_tsdb = today_tsdb + timedelta(days=1)

    # Transaction that are not "PAY" will not be screened for this rule.
    # Nor will there be any cases for non-PAY transactions.
    # Also do not screen if transaction is dated outside lookback day range.
    if transaction.transaction_type != "PAY" or transaction.transaction_timestamp >= nextday or transaction.transaction_timestamp < start_date:
        return case_ids

    # Implement Screening Logic
    for subject in [subject_1, subject_2]:
        if subject:
            screen_query = daily_transactions_summarized_view.objects.values(
                'bucket_id').annotate(total=Sum('amount_total')).filter(
                    Q(sender_id=subject.client_subject_id)
                    | Q(receiver_id=subject.client_subject_id)).filter(
                        transaction_type='PAY').filter(
                            Q(day__gte=start_date_tsdb),
                            Q(day__lt=nextday_tsdb)).annotate(
                                total=Sum('amount_total'))

            if len(screen_query) > 0 and (screen_query[0]['total'] >
                                          aggregated_amount_exceeds):
                # Refactor
                query_existing_case = check_if_case_exist_for_subject(
                    ins_rule, bucket, subject)
                if len(query_existing_case) > 0:
                    case_ids.extend(
                        handle_add_transaction_to_transaction_case(
                            transaction_id, query_existing_case, bucket))
                else:
                    initial_records = Transaction.objects.values_list(
                        'client_transaction_id', flat=True).filter(
                            (Q(cp_a_id=subject.client_subject_id)
                             | Q(cp_b_id=subject.client_subject_id)),
                            transaction_type='PAY',
                            transaction_timestamp__lt=nextday,
                            transaction_timestamp__gte=start_date,
                            bucket=bucket)
                    for i, record in enumerate(initial_records):
                        # First loop - create new case for base transaction
                        if i == 0:
                            new_case = handle_create_case(
                                transaction_id, ins_rule, bucket, "New",
                                subject)
                            case_ids.append(new_case)
                        handle_add_transaction_to_transaction_case(
                            record, case_ids, bucket)

    # [Check]: Do existing cases for cp_a and cp_b have to be returned to function call?
    return list(set(case_ids))


def check_rule_6(transaction_id, ins_rule, bucket_id):
    '''
        Check rule 6
        Flow:
        - Step 1: Extract rule_parameters
        - Step 2: get daily_transactions -> get total_amount + total_count -> caculate `average_amount` = (total_amount/total_count)
        - Step 3: Get transactions from `loop_back_days` to `today`
        - Step 4: Check `transaction.amount` > `amount_exceed_baseline_by_number_of_times` * `average_amount`
        - Step 5: (True) Create new case (if not exist) and add transaction_cases (if not exist)
    '''
    log_info(logger, "tasks/check_rule_6", "--- Start Checking Rule 6 ---")
    from bucket.models import Transaction, Bucket
    from bucket.models import daily_transactions_summarized_view
    import dateutil.relativedelta
    case_ids = []

    # Extract the fields required for screening
    rule_query = ins_rule.rule_parameters
    exeec_amount_number_of_times = rule_query[
        "amount_exceed_baseline_by_number_of_times"]
    lookback_months = rule_query["lookback_months_to_determine_baseline"]
    lookback_days = rule_query["lookback_days"]
    # Fetch the relevant records
    ins_bucket = Bucket.objects.filter(id=bucket_id).first()
    base_transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id, bucket=ins_bucket).first()

    # Obtain necessary time-related information.
    daily_transactions = list()
    today = localtime(now())

    start_date = localtime(today - timedelta(days=lookback_days)).replace(
        hour=0, minute=0, second=0, microsecond=0)
    nextday = localtime(today + timedelta(days=1)).replace(hour=0,
                                                           minute=0,
                                                           second=0,
                                                           microsecond=0)
    baseline_start_date = localtime(today -
                                    dateutil.relativedelta.relativedelta(
                                        months=lookback_months)).replace(
                                            hour=0,
                                            minute=0,
                                            second=0,
                                            microsecond=0)

    today_tsdb = localtime(today).replace(
        hour=0, minute=0, second=0,
        microsecond=0) + timedelta(hours=HOUR_OFFSET_TZ)
    baseline_start_date_tsdb = localtime(today_tsdb -
                                         dateutil.relativedelta.relativedelta(
                                             months=lookback_months))
    nextday_tsdb = today_tsdb + timedelta(days=1)

    days_used_to_determine_baseline = today_tsdb - baseline_start_date_tsdb

    # Do not screen if transaction is dated outside lookback day range.
    if base_transaction.transaction_timestamp >= nextday or base_transaction.transaction_timestamp < start_date:
        return case_ids

    # Check for both 'ends' of the transaction, where provided (is not null)
    for subject in [base_transaction.cp_a, base_transaction.cp_b]:
        if subject:
            relevant_transactions = Transaction.objects.filter(
                (Q(cp_a=subject) | Q(cp_b=subject)),
                transaction_timestamp__lt=nextday,
                transaction_timestamp__gte=start_date,
                bucket=ins_bucket)
            # Compute the summation of transaction by Subject over baseline period to determine baseline amount
            daily_transactions = daily_transactions_summarized_view.objects.values_list(
                'amount_total').filter(
                    Q(sender_id=subject.client_subject_id)
                    | Q(receiver_id=subject.client_subject_id)).filter(
                        bucket_id=bucket_id,
                        day__lt=nextday_tsdb,
                        day__gte=baseline_start_date_tsdb)
            total_amount = float()
            for i in daily_transactions:
                total_amount += i[0]
            if total_amount > 0:
                average_amount = (total_amount /
                                  days_used_to_determine_baseline.days)
            else:
                # As communicated with Dan, if baseline is 0, flag any transaction that comes in.
                # We mock a fake value here as 0 multiplied by any amount will still be 0 (won't flag as sudden high)
                average_amount = 0.00000001
            for transaction in relevant_transactions:
                if transaction.amount > (average_amount *
                                         exeec_amount_number_of_times):
                    query_cases = check_if_case_exist_for_subject(
                        ins_rule, ins_bucket, subject)

                    # Have case in same day, same rule, same bucket, same subject
                    if len(query_cases) > 0 and query_cases != None:
                        case_ids_temp = handle_add_transaction_to_transaction_case(
                            transaction.client_transaction_id, query_cases,
                            ins_bucket)
                        case_ids.extend(case for case in case_ids_temp
                                        if case not in case_ids)
                    else:

                        # If no case created for Subject 'today' with same rule, same bucket, create case.
                        suspicious_records = Transaction.objects.filter(
                            (Q(cp_a=subject) | Q(cp_b=subject)),
                            bucket=ins_bucket,
                            transaction_timestamp__lt=nextday,
                            transaction_timestamp__gte=baseline_start_date
                        ).values_list('client_transaction_id', flat=True)

                        for i, record in enumerate(suspicious_records):
                            # If create case, get all of the transactions used to form the case and load them into the bucket.
                            if i == 0:
                                new_case = handle_create_case(
                                    record, ins_rule, ins_bucket, "New",
                                    subject)
                                case_ids.append(new_case)

                            handle_add_transaction_to_transaction_case(
                                record, case_ids, ins_bucket)
                        # end for - move to next record. (extraction query for initial case creation)
                    # end if else (case creation or update logic)
            # else: do nth (no hit on rule)
        # else: move to next Subject (subject is null)
    # End for (move to next subject)
    return list(set(case_ids))


def check_rule_7(transaction_id, ins_rule, bucket_id):
    '''
    1. Find base transaction
    2. Call microservice to check if country for transaction by cp_a and b is tax haven
    3. If either country is a tax haven, create case for BOTH counter parties
    4. If both sides are tax haven, create only one case for EACH of the counterparties
    5. lookback_days logic to be confirmed (Confirm to remove by Dan, will need to update Sofia)
    '''
    log_info(logger, "tasks/check_rule_7", "--- Start Checking Rule 7 ---")
    from bucket.models import Transaction, Bucket
    from django.conf import settings
    from requests.adapters import HTTPAdapter
    from requests.packages.urllib3.util.retry import Retry
    import urllib.parse
    import requests

    transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id,
        bucket=Bucket.objects.get(id=bucket_id)).first()
    bucket = Bucket.objects.get(id=bucket_id)

    subject_1 = transaction.cp_a
    subject_2 = transaction.cp_b
    a_country = transaction.cp_a_country
    b_country = transaction.cp_b_country

    # Gets a queryset of the cases involving either subjects.
    # Placeholder to return to calls to check_rule_7() for any new cases created by the transaction
    case_ids = list()

    # We screen by Subjects in transaction as it is possible where the person isn't known, but the country is known
    for country in [a_country, b_country]:
        if country:
            MICROSERVICE_URL = settings.MICROSERVICE_PROD_URL if settings.ENV == "PROD" else settings.MICROSERVICE_DEV_URL

            retry_strategy = Retry(total=3,
                                   status_forcelist=[429, 500, 502, 503, 504],
                                   method_whitelist=["HEAD", "GET", "OPTIONS"])
            adapter = HTTPAdapter(max_retries=retry_strategy)
            http = requests.Session()
            http.mount("https://", adapter)
            response = http.get("{0}/risk/high_risk_aml_tax_haven/{1}".format(
                MICROSERVICE_URL, urllib.parse.quote(country)))

            if response.status_code != 200:
                eu_tax = -1
            else:
                response_json = response.json()
                eu_tax = response_json['eu_tax']
            # A country is a tax haven if eu_tax scoring is 0
            if eu_tax == 0:
                subject = subject_2
                if transaction.transaction_type == "DEPOSIT" and subject_2 is not None:
                    pass
                elif subject_1 is not None:  #And that Transaction type is [PAY] or [WITHDRAW]
                    subject = subject_1

                query_existing_cases = check_if_case_exist_for_subject(
                    ins_rule, bucket, subject)
                if len(query_existing_cases) > 0:
                    case_ids_temp = handle_add_transaction_to_transaction_case(
                        transaction_id, query_existing_cases, bucket)
                    case_ids.extend(case for case in case_ids_temp
                                    if case not in case_ids)
                else:
                    new_case = handle_create_case(transaction_id, ins_rule,
                                                  bucket, "New", subject)
                    case_ids.append(new_case)

    return list(set(case_ids))


def check_rule_8(transaction_id, ins_rule, bucket_id):
    '''
    1. Find base transaction
    2. Call microservice to check if country for transaction by cp_a and b is tax haven
    3. If either country is a tax haven, create case for BOTH counter parties
    4. If both sides are tax haven, create only one case for EACH of the counterparties
    5. lookback_days logic to be confirmed (Confirm to remove by Dan, will need to update Sofia)
    '''
    log_info(logger, "tasks/check_rule_8", "--- Start Checking Rule 8 ---")
    from bucket.models import Transaction, Bucket
    from django.conf import settings
    from requests.adapters import HTTPAdapter
    from requests.packages.urllib3.util.retry import Retry
    import urllib.parse
    import requests

    transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id,
        bucket=Bucket.objects.get(id=bucket_id)).first()
    bucket = Bucket.objects.get(id=bucket_id)

    subject_1 = transaction.cp_a
    subject_2 = transaction.cp_b
    a_country = transaction.cp_a_country
    b_country = transaction.cp_b_country

    # Placeholder to return to calls to check_rule_8() for any new cases created by the transaction
    case_ids = list()
    # We screen by Subjects in transaction as it is possible where the person isn't known, but the country is known
    for country in [a_country, b_country]:
        if country:
            MICROSERVICE_URL = settings.MICROSERVICE_PROD_URL if settings.ENV == "PROD" else settings.MICROSERVICE_DEV_URL
            response = requests.get(
                "{0}/risk/high_risk_aml_tax_haven/{1}".format(
                    MICROSERVICE_URL, urllib.parse.quote(country)))

            retry_strategy = Retry(total=3,
                                   status_forcelist=[429, 500, 502, 503, 504],
                                   method_whitelist=["HEAD", "GET", "OPTIONS"])
            adapter = HTTPAdapter(max_retries=retry_strategy)
            http = requests.Session()
            http.mount("https://", adapter)
            response = http.get("{0}/risk/high_risk_aml_tax_haven/{1}".format(
                MICROSERVICE_URL, urllib.parse.quote(country)))

            if response.status_code == 200:
                fatf = response.json()['fatf']
                uk_sanction = response.json()['uk_sanction']
                un_sanction = response.json()['un_sanction']
                ofac_sanction = response.json()['ofac_sanction']
            else:
                continue

            high_risk = False
            if 0.0 in [fatf, uk_sanction, un_sanction, ofac_sanction]:
                high_risk = True

            if high_risk:
                subject = subject_2
                if transaction.transaction_type == "DEPOSIT" and subject_2 is not None:
                    pass
                elif subject_1 is not None:  #And that Transaction type is [PAY] or [WITHDRAW]
                    subject = subject_1

                query_existing_cases = check_if_case_exist_for_subject(
                    ins_rule, bucket, subject)
                if len(query_existing_cases) > 0:
                    case_ids_temp = handle_add_transaction_to_transaction_case(
                        transaction_id, query_existing_cases, bucket)
                    case_ids.extend(case for case in case_ids_temp
                                    if case not in case_ids)
                else:
                    new_case = handle_create_case(transaction_id, ins_rule,
                                                  bucket, "New", subject)
                    case_ids.append(new_case)

    return list(set(case_ids))


def check_rule_9(transaction_id, ins_rule, bucket_id):
    '''
    Flags beneficiary who receives more than X dollars in total over past X days from DEPOSIT transactions
    '''
    log_info(logger, "tasks/check_rule_9", "--- Start Checking Rule 9 ---")
    from bucket.models import Transaction, Bucket, daily_withdraw_summarized_view
    from django.db.models import Q

    case_ids = list()
    transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id,
        bucket=Bucket.objects.get(id=bucket_id)).first()
    bucket = Bucket.objects.get(id=bucket_id)
    bene_client_id = transaction.cp_a

    rule_query = ins_rule.rule_parameters
    aggregated_withdrawal_exceeds = rule_query["aggregated_withdrawal_exceeds"]
    lookback_days = rule_query["lookback_days"]

    # We check only for remitter
    today = localtime(now())
    start_date = localtime(today - timedelta(days=lookback_days)).replace(
        hour=0, minute=0, second=0, microsecond=0)
    nextday = localtime(today + timedelta(days=1)).replace(hour=0,
                                                           minute=0,
                                                           second=0,
                                                           microsecond=0)

    today_tsdb = localtime(today).replace(
        hour=0, minute=0, second=0,
        microsecond=0) + timedelta(hours=HOUR_OFFSET_TZ)
    start_date_tsdb = today_tsdb - timedelta(days=lookback_days)
    nextday_tsdb = today_tsdb + timedelta(days=1)

    if transaction.transaction_type != "WITHDRAW" or transaction.transaction_timestamp >= nextday or transaction.transaction_timestamp < start_date:
        return case_ids
    else:
        # Retrieves total withdrawal based on daily basis, then sum up to get aggregated amount
        daily_transactions = daily_withdraw_summarized_view.objects.values_list(
            'amount_total').filter(
                Q(sender_id=bene_client_id.client_subject_id),
                bucket_id=bucket_id,
                day__lt=nextday_tsdb,
                day__gte=start_date_tsdb)
        total_amount = float()
        for i in daily_transactions:
            total_amount += i[0]

        if total_amount > aggregated_withdrawal_exceeds:
            # Gets a queryset of the cases involving remitter
            # # Ideally and in a real scenario, this should return only 1 record at max.
            query_existing_cases_subject = check_if_case_exist_for_subject(
                ins_rule, bucket, bene_client_id)

            if len(query_existing_cases_subject) > 0:
                case_ids_temp = handle_add_transaction_to_transaction_case(
                    transaction_id, query_existing_cases_subject, bucket)
                case_ids.extend(case for case in case_ids_temp
                                if case not in case_ids)
            else:
                initial_records = Transaction.objects.filter(
                    transaction_type='WITHDRAW',
                    transaction_timestamp__lt=nextday,
                    transaction_timestamp__gte=start_date,
                    cp_a=bene_client_id,
                    bucket=bucket).values_list('client_transaction_id',
                                               flat=True)

                # Load any historical record that resulted in the hit to trigger for case creation (assuming no case was created previously)
                for i, record in enumerate(initial_records):
                    # First loop - create new case for base transaction
                    if i == 0:
                        new_case = handle_create_case(transaction_id, ins_rule,
                                                      bucket, "New",
                                                      bene_client_id)
                        # To reach the for loop this line, case_id needs to be empty previously.
                        # Thus no need for an additional list.
                        case_ids.append(new_case)

                    handle_add_transaction_to_transaction_case(
                        record, case_ids, bucket)

    return list(set(case_ids))


def check_rule_10(transaction_id, ins_rule, bucket_id):
    '''
    Flags beneficiary who receives more than X dollars in total over past X days from DEPOSIT transactions
    '''
    log_info(logger, "tasks/check_rule_10", "--- Start Checking Rule 10 ---")
    from bucket.models import Transaction, Bucket, daily_deposit_summarized_view
    from django.db.models import Q

    case_ids = list()
    transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id,
        bucket=Bucket.objects.get(id=bucket_id)).first()
    bucket = Bucket.objects.get(id=bucket_id)
    bene_client_id = transaction.cp_b

    rule_query = ins_rule.rule_parameters
    aggregated_deposit_exceeds = rule_query["aggregated_deposit_exceeds"]
    lookback_days = rule_query["lookback_days"]

    # We check only for beneficiary
    today = localtime(now())
    start_date = localtime(today - timedelta(days=lookback_days)).replace(
        hour=0, minute=0, second=0, microsecond=0)
    nextday = localtime(today + timedelta(days=1)).replace(hour=0,
                                                           minute=0,
                                                           second=0,
                                                           microsecond=0)

    today_tsdb = localtime(today).replace(
        hour=0, minute=0, second=0,
        microsecond=0) + timedelta(hours=HOUR_OFFSET_TZ)
    start_date_tsdb = today_tsdb - timedelta(days=lookback_days)
    nextday_tsdb = today_tsdb + timedelta(days=1)

    if transaction.transaction_type != "DEPOSIT" or transaction.transaction_timestamp >= nextday or transaction.transaction_timestamp < start_date:
        return case_ids
    else:
        # Compute the summation of transaction by Subject over baseline period to determine baseline amount
        daily_transactions = daily_deposit_summarized_view.objects.values_list(
            'amount_total').filter(
                Q(receiver_id=bene_client_id.client_subject_id),
                bucket_id=bucket_id,
                day__lt=nextday_tsdb,
                day__gte=start_date_tsdb)
        total_amount = float()
        for i in daily_transactions:
            total_amount += i[0]

        if total_amount > aggregated_deposit_exceeds:
            # Gets a queryset of the cases involving beneficiary
            # Ideally and in a real scenario, this should return only 1 record at max.
            query_existing_cases_subject = check_if_case_exist_for_subject(
                ins_rule, bucket, bene_client_id)
            if len(query_existing_cases_subject) > 0:
                case_ids_temp = handle_add_transaction_to_transaction_case(
                    transaction_id, query_existing_cases_subject, bucket)
                case_ids.extend(case for case in case_ids_temp
                                if case not in case_ids)
            else:
                initial_records = Transaction.objects.filter(
                    transaction_type='DEPOSIT',
                    transaction_timestamp__lt=nextday,
                    transaction_timestamp__gte=start_date,
                    cp_b=bene_client_id,
                    bucket=bucket).values_list('client_transaction_id',
                                               flat=True)

                # Load any historical record that resulted in the hit to trigger for case creation (assuming no case was created previously)
                for i, record in enumerate(initial_records):
                    # First loop - create new case for base transaction
                    if i == 0:
                        new_case = handle_create_case(transaction_id, ins_rule,
                                                      bucket, "New",
                                                      bene_client_id)
                        # To reach the for loop this line, case_id needs to be empty previously.
                        # Thus no need for an additional list.
                        case_ids.append(new_case)

                    handle_add_transaction_to_transaction_case(
                        record, case_ids, bucket)
    return list(set(case_ids))


def check_rule_11(transaction_id, ins_rule, bucket_id):
    '''
        Check rule 11
        Flow:
        - Step 1: Get `base_transaction` | Get `subject_1`, `subject_2` from `base_transaction`
        - Step 2: Get `list_blacknames`
        - Step 3:
            - Step 3.1: Check `subject_1.name` in `list_blacknames`
                - Step 4.1: (True) Create new case (if not exist) and add transaction_cases (if not exist)
            - Step 3.2: `subject_2.name` in `list_blacknames`
                - Step 4.2: (True) Create new case (if not exist) and add transaction_cases (if not exist)

    '''
    log_info(logger, "tasks/check_rule_11", "--- Start Checking Rule 11 ---")
    from screening.models import Blacklist
    from bucket.models import Transaction, Bucket
    case_ids = []
    ins_bucket = Bucket.objects.filter(id=bucket_id).first()
    # Step 1:
    base_transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id, bucket=ins_bucket).first()
    subject_1 = base_transaction.cp_a
    subject_2 = base_transaction.cp_b
    rmt_name = subject_1.subject_name if subject_1 else ""
    bene_name = subject_2.subject_name if subject_2 else ""
    # Step 2:
    list_blacknames = Blacklist.objects.all().values('name')
    # Step 3:
    for black_name in list_blacknames:
        case_status = "New"
        # Step 3.1
        if rmt_name == black_name["name"] or bene_name == black_name["name"]:
            subject = subject_2
            if base_transaction.transaction_type == "DEPOSIT" and subject_2 is not None:
                pass
            elif subject_1 is not None:  #And that Transaction type is [PAY] or [WITHDRAW]
                subject = subject_1

            query_cases_subject = check_if_case_exist_for_subject(
                ins_rule, ins_bucket, subject)
            # Step 4.1 | Exist case with subject_1
            if len(query_cases_subject) > 0:
                case_ids_subject = handle_add_transaction_to_transaction_case(
                    transaction_id, query_cases_subject, ins_bucket)
                case_ids.extend(case for case in case_ids_subject
                                if case not in case_ids)
            else:
                case_id_created = handle_create_case(transaction_id, ins_rule,
                                                     ins_bucket, case_status,
                                                     subject)
                case_ids.append(case_id_created)
    # Remove duplicate if exist
    return list(set(case_ids))


def check_rule_12(transaction_id, ins_rule, bucket_id):
    '''
        Check rule 12
        Flow (Refer to comments in https://cynopsis.atlassian.net/browse/PAPS-128?focusedCommentId=18931):
        - Check if transaction is of type “WITHDRAW”. If so, proceed with screening for cp_a only (Transactions that are “PAY” or “DEPOSIT” will NOT be screened)
        - Check the total deposits made by the customer over the past few days (determined by lookback_days).
        - If the counter party’s aggregated deposit value (in step2) is above the threshold set in the rule’s setting’s Threshold of aggregated amount field, proceed to next layer 
            (Otherwise, no further screening done)
        - For the latest transaction uploaded to athena,  check if the amount is higher than aggreated_deposited_value multiplied by (1+ (variance_amount/100) )
            --> If so, flag the transaction as a case. 
            --> Otherwise, no case will be created.
    '''
    log_info(logger, "tasks/check_rule_12", "--- Start Checking Rule 12 ---")
    from bucket.models import daily_deposit_summarized_view, Transaction, Bucket, daily_withdraw_summarized_view
    from django.db.models import Q

    bucket = Bucket.objects.get(id=bucket_id)
    transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id, bucket=bucket).first()
    subject_1 = transaction.cp_a

    case_ids = list()

    rule_query = ins_rule.rule_parameters
    variance_percent = rule_query["variance_percent"]
    aggregated_amount_exceeds = rule_query["aggregated_amount_exceeds"]
    lookback_days = rule_query["lookback_days"]

    today = localtime(now())
    start_date = localtime(today - timedelta(days=lookback_days)).replace(
        hour=0, minute=0, second=0, microsecond=0)
    nextday = localtime(today + timedelta(days=1)).replace(hour=0,
                                                           minute=0,
                                                           second=0,
                                                           microsecond=0)

    today_tsdb = localtime(today).replace(
        hour=0, minute=0, second=0,
        microsecond=0) + timedelta(hours=HOUR_OFFSET_TZ)
    start_date_tsdb = today_tsdb - timedelta(days=lookback_days)
    nextday_tsdb = today_tsdb + timedelta(days=1)

    # Transaction that are not "WITHDRAW" will not be screened for this rule.
    # Also do not screen if transaction is dated outside lookback day range.
    if transaction.transaction_type != "WITHDRAW" or transaction.transaction_timestamp >= nextday or transaction.transaction_timestamp < start_date:
        return case_ids

    # Check the [total deposit amount] made by the customer over the past few days (determined by lookback days)
    screen_deposits = daily_deposit_summarized_view.objects.values_list(
        'amount_total').filter(Q(receiver_id=subject_1.client_subject_id),
                               bucket_id=bucket_id,
                               day__lt=nextday_tsdb,
                               day__gte=start_date_tsdb)
    total_deposited_amount = float()
    for i in screen_deposits:
        total_deposited_amount += i[0]

    # Check the [total withdrawn amount] made by the customer over the past few days (also determined by LB days)
    screen_withdrawals = daily_withdraw_summarized_view.objects.values_list(
        'amount_total').filter(Q(sender_id=subject_1.client_subject_id),
                               bucket_id=bucket_id,
                               day__lt=nextday_tsdb,
                               day__gte=start_date_tsdb)
    total_withdrawn_amount = float()
    for i in screen_withdrawals:
        total_withdrawn_amount += i[0]
    # Checks if there were any withdrawal or deposits made by the Subject over the lookback days
    # Also check if the total deposited amount exceeds threshold. (If not, no need to screen)
    if len(screen_deposits) > 0 and len(
            screen_withdrawals
    ) > 0 and total_deposited_amount > aggregated_amount_exceeds:
        threshold_percentage = variance_percent / 100.0
        threshold_amount = total_deposited_amount * threshold_percentage
        if total_withdrawn_amount > threshold_amount:
            query_existing_case = check_if_case_exist_for_subject(
                ins_rule, bucket, subject_1)
            if len(query_existing_case) > 0:
                case_ids.extend(
                    handle_add_transaction_to_transaction_case(
                        transaction_id, query_existing_case, bucket))
            else:
                initial_records_deposit = Transaction.objects.values_list(
                    'client_transaction_id',
                    flat=True).filter(cp_b_id=subject_1,
                                      transaction_type='DEPOSIT',
                                      transaction_timestamp__lt=nextday,
                                      transaction_timestamp__gte=start_date,
                                      bucket=bucket)
                for i, record in enumerate(initial_records_deposit):
                    # First loop - create new case for base transaction
                    if i == 0:
                        new_case = handle_create_case(transaction_id, ins_rule,
                                                      bucket, "New", subject_1)
                        case_ids.append(new_case)
                    handle_add_transaction_to_transaction_case(
                        record, case_ids, bucket)

                initial_records_withdraw = Transaction.objects.values_list(
                    'client_transaction_id',
                    flat=True).filter(cp_a_id=subject_1,
                                      transaction_type='WITHDRAW',
                                      transaction_timestamp__lt=nextday,
                                      transaction_timestamp__gte=start_date,
                                      bucket=bucket)

                # Include all of the transactions that forms up the aggregated withdrawal to the case
                # This will include the latest record which triggers this screening.
                for i, record in enumerate(initial_records_withdraw):
                    handle_add_transaction_to_transaction_case(
                        record, case_ids, bucket)
        # If counter party's deposited amount is not above [aggregated_amount_exceed] set in rule parameter,
        # no need to screen
    return list(set(case_ids))


def check_rule_13(transaction_id, ins_rule, bucket_id):
    log_info(logger, "tasks/check_rule_13", "--- Start Checking Rule 13 ---")
    from screening.models import Blacklist
    from bucket.models import Transaction, Bucket
    from bucket.models import daily_transactions_summarized_view
    import dateutil.relativedelta
    case_ids = []

    # Extract the fields required for screening
    rule_query = ins_rule.rule_parameters
    exeec_amount_number_of_times = rule_query[
        "amount_exceed_baseline_by_number_of_times"]
    lookback_months = rule_query["lookback_months_to_determine_baseline"]
    lookback_days = rule_query["lookback_days"]
    # Fetch the relevant records
    ins_bucket = Bucket.objects.filter(id=bucket_id).first()
    base_transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id, bucket=ins_bucket).first()

    # Obtain necessary time-related information.
    daily_transactions = list()
    today = localtime(now())
    start_date = localtime(today - timedelta(days=lookback_days)).replace(
        hour=0, minute=0, second=0, microsecond=0)
    nextday = localtime(today + timedelta(days=1)).replace(hour=0,
                                                           minute=0,
                                                           second=0,
                                                           microsecond=0)
    baseline_start_date = localtime(today -
                                    dateutil.relativedelta.relativedelta(
                                        months=lookback_months)).replace(
                                            hour=0,
                                            minute=0,
                                            second=0,
                                            microsecond=0)

    today_tsdb = localtime(today).replace(
        hour=0, minute=0, second=0,
        microsecond=0) + timedelta(hours=HOUR_OFFSET_TZ)
    nextday_tsdb = today_tsdb + timedelta(days=1)
    baseline_start_date_tsdb = localtime(today_tsdb -
                                         dateutil.relativedelta.relativedelta(
                                             months=lookback_months))
    days_used_to_determine_baseline = today_tsdb - baseline_start_date_tsdb

    # Do not screen if transaction is dated outside lookback day range.
    if base_transaction.transaction_timestamp >= nextday or base_transaction.transaction_timestamp < start_date:
        return case_ids

    list_blacknames = Blacklist.objects.all().values('name')
    # Check for both 'ends' of the transaction, where provided (is not null)
    for subject in [base_transaction.cp_a, base_transaction.cp_b]:
        if subject:
            # Check subject_name in list blacknames
            is_subject_in_black_name = False
            for black_name in list_blacknames:
                if black_name["name"] == subject.subject_name:
                    is_subject_in_black_name = True
            # Continue screening with subject_name in list_blacknames
            if is_subject_in_black_name:
                relevant_transactions = Transaction.objects.filter(
                    Q(cp_a=subject) | Q(cp_b=subject),
                    transaction_timestamp__lt=nextday,
                    transaction_timestamp__gte=start_date,
                    bucket=ins_bucket)
                # Compute the summation of transaction by Subject over baseline period to determine baseline amount
                daily_transactions = daily_transactions_summarized_view.objects.values_list(
                    'amount_total').filter(
                        Q(sender_id=subject.client_subject_id)
                        | Q(receiver_id=subject.client_subject_id)).filter(
                            bucket_id=bucket_id,
                            day__lt=nextday_tsdb,
                            day__gte=baseline_start_date_tsdb)
                total_amount = float()
                for i in daily_transactions:
                    total_amount += i[0]
                if total_amount > 0:
                    average_amount = (total_amount /
                                      days_used_to_determine_baseline.days)
                else:
                    # As communicated with Dan, if baseline is 0, flag any transaction that comes in.
                    # We mock a fake value here as 0 multiplied by any amount will still be 0 (won't flag as sudden high)
                    average_amount = 0.00000001
                for transaction in relevant_transactions:
                    if transaction.amount > (average_amount *
                                             exeec_amount_number_of_times):

                        query_cases = check_if_case_exist_for_subject(
                            ins_rule, ins_bucket, subject)

                        # Have case in same day, same rule, same bucket, same subject
                        if len(query_cases) > 0 and query_cases != None:
                            case_ids_temp = handle_add_transaction_to_transaction_case(
                                transaction.client_transaction_id, query_cases,
                                ins_bucket)
                            case_ids.extend(case for case in case_ids_temp
                                            if case not in case_ids)
                        else:
                            # If no case created for Subject 'today' with same rule, same bucket, create case.
                            suspicious_records = Transaction.objects.filter(
                                Q(cp_a=subject) | Q(cp_b=subject)
                            ).filter(
                                bucket=ins_bucket,
                                transaction_timestamp__lt=nextday,
                                transaction_timestamp__gte=baseline_start_date
                            ).values_list('client_transaction_id', flat=True)

                            for i, record in enumerate(suspicious_records):
                                # If create case, get all of the transactions used to form the case and load them into the bucket.
                                if i == 0:
                                    new_case = handle_create_case(
                                        record, ins_rule, ins_bucket, "New",
                                        subject)
                                    case_ids.append(new_case)

                                handle_add_transaction_to_transaction_case(
                                    record, case_ids, ins_bucket)
                            # end for - move to next record. (extraction query for initial case creation)
                        # end if else (case creation or update logic)
                    # else: do nth (no hit on rule)
            # else: move to next Subject (subject is null)
        # End for (move to next subject)
    return list(set(case_ids))


def check_rule_14(transaction_id, ins_rule, bucket_id):
    log_info(logger, "tasks/check_rule_14", "--- Start Checking Rule 14 ---")
    from bucket.models import Transaction, Bucket, daily_transactions_summarized_view
    from django.db.models import Sum

    # Extract the fields required for screening
    rule_query = ins_rule.rule_parameters
    aggregated_amount_exceeds = rule_query["aggregated_amount_exceeds"]
    lookback_days = rule_query["lookback_days"]
    minimum_number_of_transactions = rule_query["minimum_transaction_count"]

    case_ids = []
    bucket = Bucket.objects.filter(id=bucket_id).first()
    transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id, bucket=bucket).first()

    # Obtain necessary time-related information.
    today = localtime(now())
    start_date = localtime(today - timedelta(days=lookback_days)).replace(
        hour=0, minute=0, second=0, microsecond=0)
    nextday = localtime(today + timedelta(days=1)).replace(hour=0,
                                                           minute=0,
                                                           second=0,
                                                           microsecond=0)
    today_tsdb = localtime(today).replace(
        hour=0, minute=0, second=0,
        microsecond=0) + timedelta(hours=HOUR_OFFSET_TZ)
    nextday_tsdb = today_tsdb + timedelta(days=1)
    start_date_tsdb = today_tsdb - timedelta(days=lookback_days)
    if transaction.transaction_type != "PAY" or transaction.transaction_timestamp >= nextday or transaction.transaction_timestamp < start_date:
        return []

    receiver = transaction.cp_b

    daily_transactions = daily_transactions_summarized_view.objects.values('bucket_id') \
        .filter(
        bucket_id=bucket_id,
        transaction_type='PAY',
        receiver_id=receiver.client_subject_id,
        day__lt=nextday_tsdb,
        day__gte=start_date_tsdb) \
        .annotate(sum_total=Sum('amount_total'), sum_count=Sum('amount_count'))
    if len(daily_transactions) > 0:
        if daily_transactions[0]['sum_total'] > aggregated_amount_exceeds \
                and daily_transactions[0]['sum_count'] >= minimum_number_of_transactions:
            query_existing_case = check_if_case_exist_for_subject(
                ins_rule, bucket, receiver)
            if len(query_existing_case) > 0:
                case_ids.extend(
                    handle_add_transaction_to_transaction_case(
                        transaction_id, query_existing_case, bucket))
            else:
                client_transaction_ids = Transaction.objects.values_list(
                    'client_transaction_id', flat=True) \
                    .filter(
                    bucket=bucket,
                    cp_b=receiver,
                    transaction_type='PAY',
                    transaction_timestamp__lt=nextday,
                    transaction_timestamp__gte=start_date)
                for i, record in enumerate(client_transaction_ids):
                    # First loop - create new case for base transaction
                    if i == 0:
                        new_case_id = handle_create_case(
                            transaction_id, ins_rule, bucket, "New", receiver)
                        case_ids.append(new_case_id)
                    handle_add_transaction_to_transaction_case(
                        record, case_ids, bucket)

    return list(set(case_ids))


def check_rule_15(transaction_id, ins_rule, bucket_id):
    log_info(logger, "tasks/check_rule_15", "--- Start Checking Rule 15 ---")
    from bucket.models import Transaction, Bucket, daily_transactions_summarized_view
    from django.db.models import Sum

    # Extract the fields required for screening
    rule_query = ins_rule.rule_parameters
    aggregated_amount_exceeds = rule_query["aggregated_amount_exceeds"]
    lookback_days = rule_query["lookback_days"]
    minimum_number_of_transactions = rule_query["minimum_transaction_count"]

    case_ids = []
    bucket = Bucket.objects.filter(id=bucket_id).first()
    transaction = Transaction.objects.filter(
        client_transaction_id=transaction_id, bucket=bucket).first()

    # Obtain necessary time-related information.
    today = localtime(now())
    start_date = localtime(today - timedelta(days=lookback_days)).replace(
        hour=0, minute=0, second=0, microsecond=0)
    nextday = localtime(today + timedelta(days=1)).replace(hour=0,
                                                           minute=0,
                                                           second=0,
                                                           microsecond=0)

    if transaction.transaction_type != "PAY" or transaction.transaction_timestamp >= nextday or transaction.transaction_timestamp < start_date:
        return []

    today_tsdb = localtime(today).replace(
        hour=0, minute=0, second=0,
        microsecond=0) + timedelta(hours=HOUR_OFFSET_TZ)
    nextday_tsdb = today_tsdb + timedelta(days=1)
    start_date_tsdb = today_tsdb - timedelta(days=lookback_days)

    if transaction.transaction_type != "PAY" or (
            transaction.transaction_timestamp >= nextday
            and transaction.transaction_timestamp < start_date):
        return []

    sender = transaction.cp_a

    daily_transactions = daily_transactions_summarized_view.objects.values('bucket_id') \
        .filter(bucket_id=bucket_id,
        transaction_type='PAY',
        sender_id=sender.client_subject_id,
        day__lt=nextday_tsdb,
        day__gte=start_date_tsdb) \
        .annotate(sum_total=Sum('amount_total'), sum_count=Sum('amount_count'))

    if len(daily_transactions) > 0:
        if daily_transactions[0]['sum_total'] > aggregated_amount_exceeds \
                and daily_transactions[0]['sum_count'] >= minimum_number_of_transactions:
            query_existing_case = check_if_case_exist_for_subject(
                ins_rule, bucket, sender)
            if len(query_existing_case) > 0:
                case_ids.extend(
                    handle_add_transaction_to_transaction_case(
                        transaction_id, query_existing_case, bucket))
            else:
                client_transaction_ids = Transaction.objects.values_list(
                    'client_transaction_id', flat=True) \
                    .filter(
                    bucket=bucket,
                    cp_a=sender,
                    transaction_type='PAY',
                    transaction_timestamp__lt=nextday,
                    transaction_timestamp__gte=start_date)
                for i, record in enumerate(client_transaction_ids):
                    # First loop - create new case for base transaction
                    if i == 0:
                        new_case_id = handle_create_case(
                            transaction_id, ins_rule, bucket, "New", sender)
                        case_ids.append(new_case_id)
                    handle_add_transaction_to_transaction_case(
                        record, case_ids, bucket)

    return list(set(case_ids))


def handle_create_case(client_transaction_id,
                       rule,
                       bucket,
                       case_status,
                       case_subject_1,
                       case_subject_2=None,
                       assignee=None):
    from case.models import Case, TransactionCase
    from screening.models import Rule, RuleDetails
    from shared.service import PusherService

    rule_number = rule.rule_number.id
    alert_risk_rating = RuleDetails.objects.get(
        id=rule.rule_number.id).alert_priority_level

    case_to_create = Case(rule=rule,
                          bucket=bucket,
                          case_status=case_status,
                          alert_risk_rating=alert_risk_rating,
                          case_subject_1=case_subject_1,
                          case_subject_2=case_subject_2,
                          assignee=assignee)
    case_to_create.save()
    transaction_id = find_transaction_id(client_transaction_id, bucket)
    TransactionCase.objects.create(case=case_to_create,
                                   transaction_id=transaction_id)
    PusherService.send_message('dashboard-cases', 'case-created',
                               'New case available. Refresh to update.')
    return case_to_create.id


def handle_add_transaction_to_transaction_case(client_transaction_id, case_ids,
                                               bucket):
    '''
        Handle add client_transaction_id to TransactionCase if not exist.
        case_ids_new<type: List>: case_ids have been added new in TransactionCase.
    '''
    from case.models import TransactionCase
    from shared.service import PusherService
    case_ids_new = []
    transaction_id = find_transaction_id(client_transaction_id, bucket)

    for case_id in case_ids:
        transaction_case, created = TransactionCase.objects.get_or_create(
            case_id=case_id, transaction_id=transaction_id)
        if created:
            PusherService.send_message(
                'case-management-transactions', str(case_id),
                'Please refresh for new transaction records.')
            case_ids_new.append(case_id)
    return case_ids_new


def check_if_case_exist_for_subject(rule, bucket, subject_1, subject_2=None):
    from case.models import Case
    from django.db.models import Q
    # find cases in database with rmt_name & bene_name & in today
    today = localtime(now()).replace(hour=0, minute=0, second=0, microsecond=0)
    nextday = today + timedelta(days=1)
    if subject_2:
        query_cases = Case.objects.filter(
            Q(rule=rule), Q(bucket=bucket), Q(created_on__gte=today),
            Q(created_on__lt=nextday), Q(case_subject_1=subject_1),
            Q(case_subject_2=subject_2)).values_list('id', flat=True)
    else:
        query_cases = Case.objects.filter(
            Q(rule=rule), Q(bucket=bucket), Q(created_on__gte=today),
            Q(created_on__lt=nextday), Q(case_subject_1=subject_1),
            Q(case_subject_2__isnull=True)).values_list('id', flat=True)
    return query_cases


def find_transaction_id(client_transaction_id, bucket):
    '''
        Find transaction_id by client_transaction_id & bucket
        return: transaction_id <int> || -1 (not exist)
    '''
    from bucket.models import Transaction
    txn = Transaction.objects.filter(
        client_transaction_id=client_transaction_id, bucket=bucket).first()
    if txn:
        return txn.id
    return -1
