from __future__ import absolute_import, unicode_literals

import os
from celery import Celery
from demo_docker.settings import CELERY_BROKER_URL, QUEUE_PREFIX, Q_CAMUNDA_TRANSACTION, Q_CASES,\
    Q_CELERY_TRANSACTION, Q_CELERY_CASES, Q_CAMUNDA_CASES
from kombu import Queue

# from tasks.tasks import screening_worker
camunda_transaction_queue = Queue(QUEUE_PREFIX + Q_CAMUNDA_TRANSACTION)
camunda_cases_queue = Queue(QUEUE_PREFIX + Q_CAMUNDA_CASES)
cases_queue = Queue(QUEUE_PREFIX + Q_CASES)
celery_transaction_queue = Queue(QUEUE_PREFIX + Q_CELERY_TRANSACTION)
celery_cases_queue = Queue(QUEUE_PREFIX + Q_CELERY_CASES)

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'demo_docker.settings')
app = Celery('demo_docker', backend="django-db", broker=CELERY_BROKER_URL)
app.config_from_object('django.conf:settings', namespace='CELERY')
# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
